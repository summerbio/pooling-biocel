function PoolingLogs()
{
    function Init()
    {
        try
        {
            ProtocolTimestamp =  getTimestamp().dateTimeFileName;
            DiscreteRackStartLog = LogPath + "Discrete Rack Start Log " + ProtocolTimestamp + ".txt";
            PooledRackStartLog = LogPath + "Pooled Rack Start Log " + ProtocolTimestamp + ".txt";
            DiscreteRackEndLog = LogPath + "Discrete Rack End Log " + ProtocolTimestamp + ".txt";
            PooledRackEndLog = LogPath + "Pooled Rack End Log " + ProtocolTimestamp + ".txt";
        }
        catch(e)
        {
            Error("PoolingLogs.Init", "Exception", e);
        }
    }

    function getTimestamp()
    {
        try
        {
            var date = new Date();
            var timeMilliSeconds = date.getTime();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var year = date.getFullYear();
            var time = date.toLocaleTimeString();
            var dateTime = month + "-" + day + "-" + year + " " + time;

            // replace ':' in time string in order to make this a valid file name
            var dateTimeFileName = dateTime.replace(":","_");
            dateTimeFileName = dateTimeFileName.replace(":","_");

            var timestamp = 
            {
                "milliSeconds": timeMilliSeconds,
                "time": time,
                "dateTime": dateTime,
                "dateTimeFileName": dateTimeFileName,
            };
            return timestamp; 
        }
        catch(e)
        {
            Error("PoolingLogs.GetTimestamp", "Exception", e);
        }

    }

    function UpdateLog(logFileName)
    {
        try
        {
            var timestamp = getTimestamp();
            var newline = plate.instance + ", " + timestamp.dateTime + "\n"; 
            FileOperations.AppendFile(logFileName, newline); 
        }
        catch(e)
        {
            Error("PoolingLogs.UpdateLog", "Exception", e);
        }
    }
}