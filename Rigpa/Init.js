try
{
	// initialize silent script fail gaurd specifically for Init.js (before E() is loaded!)
	InitSuccess = false;

	// global file paths
	ScriptPath = "c:\\vworks workspace\\Rigpa\\";
	LogPath = "C:\\vworks workspace\\Pooling Logs\\";
	BarcodeFolder = "C:\\vworks workspace\\Barcode Files\\";
	PoolingSettingsPath = "c:\\pooling-biocel-settings\\Rigpa\\PoolingSettings.js";

	// global flags
	DONE_POOLING = false;
	STILL_POOLING = true;
	LOG_PLATE_CONTROL = false;
	LOG_SCRIPT_TIMING = false;
	LOG_RACK_TIMING = true;
	SKIP_USER_PROMPTS = false;
	SKIP_WEIGH_PADS = false; // skips all weighpad tasks
	SKIP_WEIGH_PADS_WARNING = false; // still records values, but doesn't check thresholds
	WRITE_TO_LIMS = true;

	// Load pooling settings (system-specific)
	open(PoolingSettingsPath);

	// check that system-specific variables are defined
	if (typeof PoolingRatio === 'undefined')
	{
		print("PoolingRatio is undefined! Abort the protocol and contact an engineer to ensure this is defined in: " + PoolingSettingsPath);
		task.pause();
	}
	else
	{
		if (PoolingRatio > 10 || PoolingRatio < 2)
		{
			print("PoolingRatio: " + PoolingRatio + " is out of range. Abort the protocol and contact an engineer to ensure this is defined in: " + PoolingSettingsPath);
			task.pause();
		}
	}
	if (typeof SystemName === 'undefined')
	{
		print("SystemName is undefined! Abort the protocol and contact an engineer to ensure this is set properly in: " + PoolingSettingsPath);
		task.pause();
	}


	// load E( framework and initialize silent script fail gaurd
	open(ScriptPath + "Eval.js")
	silentScriptFailGuard = -1;
	previousEFrameworkExecution = "";

	// Next, load utility libraries that aren't specific to the pooling protocol
	open(ScriptPath + "FileOperations.js");
	open(ScriptPath + "SemaphoreController.js");
	open(ScriptPath + "ResourcePool.js");
	open(ScriptPath + "JSONSerializer.js");

	// Finally, load libraries that contains functions called by tasks in the pooling protocol
	open(ScriptPath + "PoolingLogs.js");
	open(ScriptPath + "PlateControl.js");
	open(ScriptPath + "DecapperChecker.js");	
	open(ScriptPath + "PoolingDecappers.js");	
	open(ScriptPath + "PoolingTips.js");
	open(ScriptPath + "TubeBarcodeChecker.js");		
	open(ScriptPath + "TubeBarcodeCheckerTasks.js");
	open(ScriptPath + "RackBarcodeChecker.js");
	open(ScriptPath + "PoolingBarcodes.js");
	open(ScriptPath + "PoolingFlowControl.js");
	open(ScriptPath + "PoolingPipetting.js");
	open(ScriptPath + "PoolingMicroFill.js");
	open(ScriptPath + "CapCarrierTasks.js");
	open(ScriptPath + "BeckhoffTasks.js");
	open(ScriptPath + "Conveyors.js");
	open(ScriptPath + "UniFiCamera.js");
	open(ScriptPath + "Form.js");


	// Initialize libraries as needed
	PoolingLogs.Init();
	RackBarcodeChecker.Init();
	PlateControl.Init();
	PoolingDecappers.Init();
	PoolingBarcodes.Init();
	PoolingPipetting.Init();
	PoolingFlowControl.Init();
	PoolingMicroFill.Init();
	UniFiCamera.Init();
	Form.Init();

	// set silent script fail gaurd specifically for Init.js to indicate loading success!
	InitSuccess = true;

	print("All scripts loaded successfully!");
}
catch(e)
{
	task.pause();
	print("Init.js: Error loading loading javascript libraries.");
}