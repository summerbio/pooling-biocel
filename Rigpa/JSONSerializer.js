function JSONSerializer()
{
    this.SerializeBase = function(o, prepend, prependIncrement, newLine)
    {
        var result = "";
        if(o == null)
            return "null";

        if(typeof o === "string")
            return "\"" + o + "\"";
        
        if(typeof o === "number")
            return o;

        if(typeof o === "boolean")
            if(o)
              return "true";
            else 
              return "false";

        if(typeof o === "object")
        {
            if(o.length === undefined)
            {
                result += "{";

                var keys = [];

                for(var x in o)
                    keys.push(x);

                for(var k = 0; k < keys.length; ++k)
                {
                  if(newLine)
                      result += "\n";

                  result += prepend + "\"" + keys[k] + "\" : " + this.SerializeBase(o[keys[k]], prepend + prependIncrement, prependIncrement, newLine);
                  
                  if(k < keys.length - 1)
                      result += ",";
                    
                }

                if(newLine)
                    result += "\n";

                result += "}"
            }
            else
            {
              //Array
              result += "[";
              
              for(var k = 0; k < o.length; ++k)
              {
                if(newLine)
                    result += "\n";

                result += prepend + this.SerializeBase(o[k], prepend + prependIncrement, prependIncrement, newLine);
                
                if(k < o.length - 1)
                    result += ",";                    
              }

              if(newLine)
                  result += "\n";

              result += prepend + "]";
            }
        }

        return result;

    }
    
    this.SerializeFormatted = function(o)
    {
        return this.SerializeBase(o, "", "\t", true);
    }

    this.Serialize = function(o)
    {
        return this.SerializeBase(o, "", "", false);
    }
}
