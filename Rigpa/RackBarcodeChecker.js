function RackBarcodeChecker()
{
	function Init()
	{
		try
		{
			RackBarcodes = {};
		}
		catch(e)
        {
            Error("RackBarcodeChecker.Initialize", "Exception", e);
        }
	}

	function isValidCapCarrierRackBarcode()
	{
		try
		{
			var capCarrierRackBarcode = plate.barcode[WEST];
			if((capCarrierRackBarcode != null) && (capCarrierRackBarcode != "") && (capCarrierRackBarcode != "No barcode"))
			{
				if ((capCarrierRackBarcode == "CAP CARRY") || (capCarrierRackBarcode == "cap carry"))
				{
					return true;
				}
			}
			return false;
		}
		catch(e)
        {
            Error("RackBarcodeChecker.isValidCapCarrierRackBarcode", "Exception", e);
        }
	}

	function isValidPooledRackBarcode()
	{
		try
		{
			var pooledRackBarcode = plate.barcode[WEST];
			if((pooledRackBarcode != null) && (pooledRackBarcode != "") && (pooledRackBarcode != "No barcode"))
			{
				if (pooledRackBarcode[0] == "P" || pooledRackBarcode[0] == "p")
				{
					return true;
				}
			}
			return false;
		}
		catch(e)
        {
            Error("RackBarcodeChecker.isValidPooledRackBarcode", "Exception", e);
        }
	}

	function isValidDiscreteRackBarcode()
	{
		try
		{
			var discreteRackBarcode = plate.barcode[WEST];
			if((discreteRackBarcode != null) && (discreteRackBarcode != "") && (discreteRackBarcode != "No barcode"))
			{
				if (discreteRackBarcode[0] != "P" && discreteRackBarcode[0] != "p")
				{
					return true;
				}
			}
			return false;
		}
		catch(e)
        {
            Error("RackBarcodeChecker.isValidDiscreteRackBarcode", "Exception", e);
        }
	}

	// barcode that begins with "X" or "x" prefix indicates that it already has RNA Buffer added
	function isNonContrivedValidationRack()
	{
		try
		{
			var discreteRackBarcode = plate.barcode[WEST];
			if((discreteRackBarcode != null) && (discreteRackBarcode != "") && (discreteRackBarcode != "No barcode"))
			{
				if (discreteRackBarcode[0] == "X" || discreteRackBarcode[0] == "x")
				{
					return true;
				}
			}
			return false;
		}
		catch(e)
        {
            Error("RackBarcodeChecker.isNonContrivedValidationRack", "Exception", e);
        }
	}

	function ClearRackBarcode()
	{
		try
		{
			if (task.isSimulatorRunning())
			{
				plate.setBarcode(WEST,"");
			}
		}
		catch(e)
        {
            Error("RackBarcodeChecker.ClearRackBarcode", "Exception", e);
        }
	}

	function LoopBeginPooled()
	{
		try
		{
			if (isValidPooledRackBarcode() || SKIP_USER_PROMPTS)
			{
				task.skip();
			}
		}
		catch(e)
        {
            Error("RackBarcodeChecker.LoopBeginPooled", "Exception", e);
        }
	}

	function LoopBeginDiscrete()
	{
		try
		{
			if (isValidDiscreteRackBarcode() || SKIP_USER_PROMPTS)
			{
				task.skip();
			}
		}
		catch(e)
        {
            Error("RackBarcodeChecker.LoopBeginDiscrete", "Exception", e);
        }
	}

	function ScanPooledRackBarcode()
	{
		try
		{
			if (task.isSimulatorRunning())
			{
				PoolingBarcodes.CreateSimPooledBarcode();
				print(plate.barcode[WEST]);
			}					
		}
		catch(e)
		{
			Error("RackBarcodeChecker.ScanPooledRackBarcode", "Exception", e);
		}
	}

	function ScanDiscreteRackBarcode()
	{
		try
		{
			if (task.isSimulatorRunning())
			{
				PoolingBarcodes.CreateSimDiscreteBarcode();
			}					
		}
		catch(e)
		{
			Error("RackBarcodeChecker.ScanDiscreteRackBarcode", "Exception", e);
		}
	}

	function ScanCapCarrierRackBarcode()
	{
		try
		{
			if (task.isSimulatorRunning())
			{
				plate.setBarcode(WEST, "CAP CARRY");
			}	
		}
		catch(e)
		{
			Error("RackBarcodeChecker.ScanCapCarrierRackBarcode", "Exception", e);
		}
	}

	function UserMessageCapCarrier()
	{
		try
		{
			if (isValidCapCarrierRackBarcode() || SKIP_USER_PROMPTS)
			{
				task.skip();
			}
		}
		catch(e)
		{
			Error("RackBarcodeChecker.UserMessageCapCarrier", "Exception", e);
		}
	}

	function UserMessagePooled()
	{
		try
		{
			if (isValidPooledRackBarcode() || SKIP_USER_PROMPTS)
			{
				task.skip();
			}
			else
			{
				newPooledRackBarcode = null;
			}
		}
		catch(e)
		{
			Error("RackBarcodeChecker.UserMessagePooled", "Exception", e);
		}
	}

	function UserMessageDiscrete()
	{
		try
		{
			if (isValidDiscreteRackBarcode() || SKIP_USER_PROMPTS)
			{
				task.skip();
			}
			else
			{
				newDiscreteRackBarcode = null;
			}
		}
		catch(e)
		{
			Error("RackBarcodeChecker.UserMessageDiscrete", "Exception", e);
		}
	}

	function LoopEndPooled()
	{
		try
		{
			if (isValidPooledRackBarcode())
			{
				task.skip();
			}
			else
			{
				plate.setBarcode(WEST, newPooledRackBarcode);
			}
		}
		catch(e)
		{
			Error("RackBarcodeChecker.LoopEndPooled", "Exception", e);
		}
	}

	function LoopEndDiscrete()
	{
		try
		{
			if (isValidDiscreteRackBarcode())
			{
				task.skip();
			}
			else
			{
				plate.setBarcode(WEST, newDiscreteRackBarcode);
			}
		}
		catch(e)
		{
			Error("RackBarcodeChecker.LoopEndDiscrete", "Exception", e);
		}
	}

	function StoreRackBarcode()
	{
		try
		{
			RackBarcodes[plate.instance] = plate.barcode[WEST];
		}
		catch(e)
		{
			Error("RackBarcodeChecker.StoreRackBarcode", "Exception", e);
		}
	}

	function ApplyRackBarcode()
	{
		try
		{
			plate.setBarcode(WEST, RackBarcodes[plate.instance]);
		}
		catch(e)
		{
			Error("RackBarcodeChecker.ApplyRackBarcode", "Exception", e);
		}
	}
}