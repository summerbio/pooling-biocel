function CapCarrierTasks()
{
	function UserPrompt()
	{
		try
		{
			if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("CapCarrierTasks.UserPrompt", "Exception", e);
		}
	}

	function HoldCapCarrier()
	{
		try
		{
			if (DONE_POOLING && PooledRacks.InProcess.length == 0)
			{
				print("Releasing cap carrier!");
			}
			else
			{
				task.repeatDelay(10);
			}
		}
		catch(e)
		{
            Error("CapCarrierTasks.HoldCapCarrier", "Exception", e);
		}
	}
}