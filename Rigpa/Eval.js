function E(code)
{
    try
    {
        //silentScriptFailGuard, a variable E() uses to watch for silent script failures
        if(silentScriptFailGuard == 1)
        {   
            //Allowing for error recovery         
            silentScriptFailGuard = 0;

            Error("E", "Previous E execution failed silently: " + previousEFrameworkExecution, null);
        }

        //Trigger silent fail guard
        silentScriptFailGuard = 1;
        previousEFrameworkExecution = code;

        if(LOG_SCRIPT_TIMING)
        {
            var eStartTimeString = GetTimestamp();
            var eStartTime = (new Date()).getTime();
        }

        // EXECUTE JAVASCRIPT!
        var result = null;
        result = eval(code);
        
        if(LOG_SCRIPT_TIMING)
        {
            var eCompleteTime = (new Date()).getTime();
            var eDuration = eCompleteTime - eStartTime;

            var eTimeDebugFile = new File();
            eTimeDebugFile.Open(LogPath + ProtocolTimestamp + " " + " ScriptTimingDebug.log");
            eTimeDebugFile.Write("\n" + plate.name + "\t" + plate.instance + "\t" + task.Tasknumber + "\t" + code.split("(")[0] + "\t" + eStartTimeString + "\t" + eDuration);
            eTimeDebugFile.Close();
        }
           
    }
    catch(e)
    {
        Error("E", "Exception while executing code: " + code, e);
    }

    silentScriptFailGuard = 0;
}

function Error(functionName, message, exception)
{
    task.pause();

    print("-------------------------------------");
    print("ERROR");
    print("Function: " + functionName);
    if(plate != null)
        print("Plate: " + plate.name + " " + plate.instance);
    if(task != null)
        print("Task: " + task.name + " " + task.description);    
    print(message);
    print("Exception:" + exception);
    print("-------------------------------------");

    if(exception != null)
        throw exception;
    else
        throw functionName + " Exception";
}
