/**************************************************************************************************
AUTHOR: 				Jeffrey Diament
DATE LAST MODIFIED: 	3/9/2021
DESCRIPTION:
TubeBarcodeChecker.js contains a code that allows operators to manually scan tube barcodes that the Ziath fails to read
TubeBarcodeChecker is a singleton used in RNA Source swimlanes of SB3+  production protocols
it is re-initialized (all non-constant properties set to null) before it's used in each swimlane

NOTE: this uses the VWorks tube barcode output file (for NEW Mirth rack-to-plate data flow)
***************************************************************************************************/
var TubeBarcodeChecker = {

    // CONSTANT: folder storing tube rack CSVs
    FOLDER_NAME         : BarcodeFolder,

    // CONTSTANTS: characteristics of VWorks scanner file output
    LINE_START          : 0,  // line index for start of data- skips header titles in file
    LINE_END            : 95, // line index for end of data
    TUBE_LOCATION_ARARY	:  ["A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12",
    						"B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12",
    						"C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10", "C11", "C12",
    						"D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12",
    						"E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "E10", "E11", "E12",
    						"F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12",
    						"G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9", "G10", "G11", "G12",
    						"H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10", "H11", "H12"],
    
    // properties specific to a given "state" of the TubeBarcodeChecker object
    rackBarcode         : null, // 1D rack barcode
    fileName            : null, // e.g., "rack_12345678.csv" 
    badBarcodeFlag      : null, // null: no info about barcodes in file; true: invalid barcode in file; false: no invalid barcode in file
    badInputFlag        : null, // null: no info about operator input barcode, true: bad input (invalid or duplicate barcode), false: good input (valid and unique barcode)
    lineArray           : null, // array containing all lines of read file  
    missingLoc          : null, // location of invalid barcode (e.g., A1)
    missingLineNum      : null, // line number in file of invalid barcode
    titleText           : null, // text to display in title of user message for inputing a new barcode
    newTubeBarcode     	: null, // new tube barcode input by operator

    /*************************************************************************************************** 	
	DESCRIPTION: scans all barcodes in file and updates singleton properties if there is a invalid barcode
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	fileName = null 
	file does not exist
	***************************************************************************************************/
    readFile            : function() {
    	if (this.fileName != null) {
	        // read file
	        var file = new File();
	        var path = this.FOLDER_NAME + this.fileName
	        if(file.Exists(path)) {
		        this.badBarcodeFlag = false // reset flag!

	            file.Open(path, 0, 0)
	            var result = file.Read()
	            this.lineArray = result.split("\n")

	            // scan each line of file and find the first bad barcode and set the singleton properties
	            for(var i=this.LINE_START; i<=this.LINE_END; i++) {
	                var barcode = this.lineArray[i]

	                // allow barcodes that start with a white space to pass through (NOT invalid)
	                if (barcode[0] == " " && barcode.length == 9) {barcode = barcode.substring(1,9)}

	                if(this.isTubeBarcodeValid(barcode)) {
	                    continue 
	                } 
	                // invalid tube barcode
	                else {
	                    this.missingLoc = this.TUBE_LOCATION_ARARY[i]
	                    this.missingLineNum = i
	                    this.badBarcodeFlag = true
	                    this.titleText =  "MISSING BARCODE AT TUBE: " + this.missingLoc
	                    print("TubeBarcodeChecker.readFile(): missing barcode/bad barcode " + barcode + " at location: " + this.missingLoc)
	                    break
	                }
	            }

	            // if full file is scanned and no bad barcode exists
	            if(this.badBarcodeFlag == false) {
	                print("TubeBarcodeChecker.readFile(): 96 good barcodes! skipping rest of TubeBarcodeChecker!!")
	            }
	            file.Close()
	        }
	        else {print("TubeBarcodeChecker.readFile(): this file does not exist:" + this.fileName)}
	    }
	    else {print("TubeBarcodeChecker.readFile(): function called with fileName=null")}
    },

    /************************************************************************************************** 	
	DESCRIPTION: if there is a invalid barcode, write new file with new barcode inserted in proper location
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	badBarcodeFlag 	!= true
	fileName 		= null 
	lineArray 		= null
	missingLineNum 	= null
	missingLoc 		= null
	file does not exist
	***************************************************************************************************/    
	writeFile           : function() {
        // only write file if there is definitely a bad barcode!
        if(this.badBarcodeFlag) {
        	if (this.fileName != null && this.lineArray != null && this.missingLineNum != null && this.newTubeBarcode !=null) {
	            var file = new File();
	            var path = this.FOLDER_NAME + this.fileName
	            if(file.Exists(path)) { 
		            file.Open(path, 1, 0) // delete contents before writing
		            this.lineArray[this.missingLineNum] = this.newTubeBarcode
		            var writeString = this.lineArray.join("\n")

		            file.Write(writeString)
		            file.Close()

		            print("TubeBarcodeChecker.writeFile(): new file written with barcode: " + this.newTubeBarcode + " at line: " + this.missingLineNum)
		        }
		        else {print("TubeBarcodeChecker.writeFile(): this file does not exist:" + this.fileName)}
		    }
		    else {print("TubeBarcodeChecker.writeFile(): function called with fileName, lineArray, missingLineNum, or newTubeBarcode = null")}
        }
        else {print("TubeBarcodeChecker.writeFile(): function called with badBarcodeFlag != true")}
    },

    /*************************************************************************************************** 	
	DESCRIPTION: check opertor input and set badInputFlag to true IF barcode is invalid OR a duplicate
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	newTubeBarcode  	= null
	lineArray 			= null
	file does not exist
	***************************************************************************************************/  
    checkInput  		: function() {
        if (this.newTubeBarcode != null) {
        	if (this.isTubeBarcodeValid(this.newTubeBarcode)) {
        		if (this.lineArray != null) {
	        		this.badInputFlag = false
		            for(var i=this.LINE_START; i<=this.LINE_END; i++) {
		                var barcode = this.lineArray[i]

		                // barcode must be a number (NOT NaN) AND have length 8
		                if(barcode == this.newTubeBarcode) {
		                    this.badInputFlag = true
		                    print("TubeBarcodeChecker.checkInput(): duplicate found of newly scanned barcode at: " + this.missingLoc)
		                    break
		                } 
		            }
		            if (this.badInputFlag == false) {print("TubeBarcodeChecker checkInput(): no duplicates found of newly scanned barcode: " + this.newTubeBarcode)}
		        }
		        else {
		        	this.badInputFlag = true
		        	print("TubeBarcodeChecker.checkInput(): function called with lineArray=null")
		        }
	        }
	        else {
	        	this.badInputFlag = true
	        	print("TubeBarcodeChecker.checkInput(): invalid newTubeBarcode (not a number of length 8)")
	        }
        }
        else {
            this.badInputFlag = true
            print("TubeBarcodeChecker.checkInput(): newTubeBarcode is null")
        }
    },

    /***************************************************************************************************	
	DESCRIPTION: check /Barcode Files/ to find latest file output by VWorks software (up to "_10.csv")
	if there are multiple copies ("..._X.csv"), the highest consecutive X less than 10 will be returned
	for ex: if there are files rack_12345678.csv, rack_12345678_1, rack_12345678_2.csv --> fileName=rack_12345678_2.csv
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	rackBarcode  	= null
	***************************************************************************************************/  
    findLatestFile      : function() {
    	if (this.rackBarcode != null) {
	        latest_fname = null
	        var file = new File()
	        var fname = "rack_" + this.rackBarcode + ".csv"
	        var path = this.FOLDER_NAME + fname

	        if(file.Exists(path)) {
	            latest_fname = fname
	        }

	        for(var i=1; i<=10; i++) {
	            fname = "rack_" + this.rackBarcode + "_" + i + ".csv"
	            path = this.FOLDER_NAME + fname
	            if(file.Exists(path)) {
	                latest_fname = fname
	            }
	            else {
	                break
	            }
	        }
	        if (latest_fname != null) {
		        this.fileName = latest_fname
		        print("TubeBarcodeChecker.findLatestFile(): latest file is: " + this.fileName)        
	        }
	        else {print("TubeBarcodeChecker.findLatestFile(): no files found for rackBarcode=" + this.rackBarcode)}
	        file.Close()
	    }
	    else {print("TubeBarcodeChecker.findLatestFile(): rackBarcode is null")}
    },

    /*************************************************************************************************** 	
	DESCRIPTION: if newTubeBarcode is not null set newTubeBarcode --> this.newTubeBarcode  
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: none
	***************************************************************************************************/        
	resetTubeBarcode     : function () {
        if (this.newTubeBarcode != null) {this.newTubeBarcode = null}
    	print("TubeBarcodeChecker.resetTubeBarcode(): newTubeBarcode set to null")
    },

    /*************************************************************************************************** 	
	DESCRIPTION: resets state of singleton by setting all properties to null
   	INPUTS: none
   	OUTPUTS: none  
	ERROR CHECKING: none
	***************************************************************************************************/     
	resetState          : function() {
        this.rackBarcode 		= null
        this.fileName 			= null
        this.badBarcodeFlag     = null
        this.badInputFlag     	= null
        this.lineArray          = null
        this.missingLoc         = null
        this.missingLineNum     = null
        this.titleText          = null
        this.newTubeBarcode     = null

        print("TubeBarcodeChecker.resetState(): state is reset")
    },

    /***************************************************************************************************	
	DESCRIPTION: output string showing current state of the singleton -- this can be printed to VWorks log
   	INPUTS: none
   	OUTPUTS: STRING  
	ERROR CHECKING: none
	***************************************************************************************************/     
	printState          : function() {
        var returnString = ""
        + "rackBarcode: " 					+ this.rackBarcode + "\n" 
        + "fileName: " 						+ this.fileName + "\n"
        + "badBarcodeFlag: " 				+ this.badBarcodeFlag + "\n"
        + "badInputFlag: " 					+ this.badInputFlag + "\n"
        + "lineArray: " 					+ this.lineArray + "\n"
        + "missingLoc: " 					+ this.missingLoc + "\n"
        + "missingLineNum: " 				+ this.missingLineNum + "\n"
        + "titleText: " 					+ this.titleText + "\n"
        + "newTubeBarcode: " 				+ this.newTubeBarcode + "\n"

        return returnString
    },

    /*************************************************************************************************** 	
	DESCRIPTION: if rackBarcode is not null set rackBarcode --> this.rackBarcode  
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	rackBarcode (function input)  	= null
	***************************************************************************************************/  
    setRackBarcode       : function (rackBarcode) {
        if (rackBarcode != null) {
            this.rackBarcode = String(rackBarcode) 
            print("TubeBarcodeChecker.setRackBarcode(): new rack barcode is: " + this.rackBarcode)    
        }     
        else {print("TubeBarcodeChecker.setRackBarcode(): rackBarcode is null")} 
    },

    /*************************************************************************************************** 	
	DESCRIPTION: if newTubeBarcode is not null set newTubeBarcode --> this.newTubeBarcode  
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	newTubeBarcode (function input)  	= null
	***************************************************************************************************/      
	setTubeBarcode       : function (newTubeBarcode) {
        if (newTubeBarcode != null) {
            this.newTubeBarcode = String(newTubeBarcode)
            print("TubeBarcodeChecker.setTubeBarcode(): newTubeBarcode is: " + this.newTubeBarcode)  
        }
        else {
            print("TubeBarcodeChecker.setnewTubeBarcode(): newTubeBarcode is null")
        }         
    },

    /*************************************************************************************************** 	
	DESCRIPTION: if tubeBarcode is valid (8 digits & 1st digit can be "P", all other digits must be numbers)  
   	INPUTS: STRING
   	OUTPUTS: BOOLEAN (true if tubeBarcode is valid, false if tubeBarcode is invalid)
	ERROR CHECKING: none
	***************************************************************************************************/
    isTubeBarcodeValid	: function (tubeBarcode) {
    	if (tubeBarcode == null)
    	{
    		return false;
    	}
    	if (tubeBarcode.length != 8)
    	{
    		return false;
    	}

    	// valid barcode must have a first digit of "P", "p", or a number
    	var firstDigit = tubeBarcode[0];
    	if (!(firstDigit == "P" || firstDigit == "p" || !isNaN(firstDigit)))
    	{
    		return false;
    	}

    	// valid barcode must have last 7 digits that are all numbers
    	var lastSevenDgits = tubeBarcode.slice(-7);
    	if (isNaN(lastSevenDgits))
    	{
    		return false;
    	}

    	return true;
    },
};
