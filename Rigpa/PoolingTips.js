function PoolingTips()
{
	//do this task only for tip boxes with an odd plate instance
	function PlaceAtShelf7()
	{
		try
		{
			if (plate.instance % 2 == 0) 
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingTips.PlaceAtShelf7", "Exception", e);
		}
	}

	//do this task only for tip boxes with an even plate instance
	function PlaceAtShelf8()
	{
		try
		{
			if (plate.instance % 2 == 1) 
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingTips.PlaceAtShelf8", "Exception", e);
		}
	}

	// if tip box has an odd plate instance, do tips on/off from Shelf 7
	// if tip box has an even plate instance, do tips on/off from Shelf 8
	function TipsOnOff()
	{
		try
		{
			if (plate.instance % 2 == 1)
			{
			    task.Location_location = "Shelf 7";
			}

			if (plate.instance % 2 == 0)
			{
			    task.Location_location = "Shelf 8";
			}
		}
		catch(e)
		{
            Error("PoolingTips.TipsOn", "Exception", e);
		}
	}
}