function PoolingMicroFill()
{
	function Init()
	{
		try
		{
			WeighPadTotalMin = 4; //[L]
			WeighPadTotalMax = 15; //[L]
			WeighPadDiffMin = 0.95 * 400/1000 * 96; //[mL]
			WeighPadDiffMax = 1.05 * 400/1000 * 96; //[mL]
			
			WeighPadTotalName = "TotalVolume";
			WeighPadDiffName = "DispenseVolume_mL";
			WeighPadDiffKeyName = "DiffKey";
		}
		catch(e)
        {
            Error("PoolingMicroFill.Init", "Exception", e);
        }
	}

	function DispenseRNABuffer()
	{
		try
		{
			if(RackBarcodeChecker.isNonContrivedValidationRack())
			{
				task.skip();
			}
		}
		catch(e)
        {
            Error("PoolingMicroFill.DispenseRNABuffer", "Exception", e);
        }
	}

	function Reserve()
	{
		try
		{
			var resource = "MicroFill-1";
			var token = "Rack " + plate.instance;
			PoolingSemaphoreController.AwaitToken(resource, token)
		}
        catch(e)
        {
             Error("PoolingMicroFill.Reserve", "Exception", e);
        }
	}

	function Release()
	{
		try
		{
			var resource = "MicroFill-1";
			var token = "Rack " + plate.instance;
			PoolingSemaphoreController.ReleaseToken(resource, token)
		}
        catch(e)
        {
             Error("PoolingMicroFill.Release", "Exception", e);
        }
	}

	function WeighPadPreDispense()
	{
		try
		{
			if(RackBarcodeChecker.isNonContrivedValidationRack())
			{
				task.skip();
			}
			else if (!SKIP_WEIGH_PADS)
			{
				if (!SKIP_WEIGH_PADS_WARNING)
				{
					task.CheckThreshold = true;
				}
				task.CheckThreshold = true;
				task.ThresholdMin = WeighPadTotalMin;
				task.ThresholdMax = WeighPadTotalMax;
				task.VariableName = WeighPadTotalName;
				task.DifferenceKey = WeighPadDiffKeyName + plate.instance;
			}
			else
			{
				task.skip();
			}
		}
        catch(e)
        {
             Error("PoolingMicroFill.WeighPadPreDispense", "Exception", e);
        }
	}

	function WeighPadPostDispense()
	{
		try
		{
			if(RackBarcodeChecker.isNonContrivedValidationRack())
			{
				task.skip();
			}
			else if (!SKIP_WEIGH_PADS)
			{
				if (!SKIP_WEIGH_PADS_WARNING)
				{
					task.CheckDifference = true;
				}
				task.DifferenceMin = WeighPadDiffMin;
				task.DifferenceMax = WeighPadDiffMax;
				task.VariableName = WeighPadTotalName;
				task.DifferenceVariableName = WeighPadDiffName;
				task.DifferenceKey = WeighPadDiffKeyName + plate.instance;
			}
			else
			{
				task.skip();
			}
		}
        catch(e)
        {
             Error("PoolingMicroFill.WeighPadPostDispense", "Exception", e);
        }
	}

	function WeighPadPreDispenseDone()
	{
		try
		{
			if(RackBarcodeChecker.isNonContrivedValidationRack())
			{
				task.skip();
			}
			else if (!SKIP_WEIGH_PADS)
			{
				if(task.isSimulatorRunning())
				{
					var simTotalVolume = WeighPadTotalMin + (WeighPadTotalMax-WeighPadTotalMin)*Math.random();
					eval(WeighPadTotalName + "=" + simTotalVolume)
				}
				DispenseVolume_mL = 0;
				Form.UpdateFormVolumesPreDispense();
			}
			else
			{
				task.skip();
			}
		}
        catch(e)
        {
             Error("PoolingMicroFill.WeighPadPreDispenseDone", "Exception", e);
        }
	}

	function WeighPadPostDispenseDone()
	{
		try
		{
			if(RackBarcodeChecker.isNonContrivedValidationRack())
			{
				task.skip();
			}
			else if (!SKIP_WEIGH_PADS)
			{
				if(task.isSimulatorRunning())
				{
					var simTotalVolume = WeighPadTotalMin + (WeighPadTotalMax-WeighPadTotalMin)*Math.random();
					var simDiffVolume = WeighPadDiffMin + (WeighPadDiffMax-WeighPadDiffMin)*Math.random()
					eval(WeighPadTotalName + "=" + simTotalVolume)
					eval(WeighPadDiffName + "=" + simDiffVolume)
				}
				Form.UpdateFormVolumesPostDispense();
			}
			else
			{
				task.skip();
			}
		}
        catch(e)
        {
             Error("PoolingMicroFill.WeighPadPostDispenseDone", "Exception", e);
        }
	}

	function FirstSpawnReleaseMicroFill()
	{
		try
		{
			// only run this task to open up the MicroFill for the first discrete rack
			if (plate.instance != 1)
			{
				task.skip();
			}
		}
		catch(e)
		{
	         Error("PoolingMicroFill.FirstSpawnReleaseMicroFill", "Exception", e);
		}
	}
}