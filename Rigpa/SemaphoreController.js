function SemaphoreController(filePath, repeatDelayPeriod, enableLogging)
{
    this.filePath = filePath;
    this.registry = {};
    this.awaitQueue = {};
    this.awaitingList = {};
    this.tokenReservationCount = {};
    this.EnableLogging = enableLogging;
    this.RepeatDelayPeriod = repeatDelayPeriod;

    this.Await = function(resource)
    {      
        var processId = plate.name + " " + plate.instance;
        
        return this.AwaitToken(resource, processId);
    }

    this.AwaitToken = function(resource, token)
    { 
        if(this.awaitQueue[resource] == null)
            this.awaitQueue[resource] = [];

        if(this.awaitingList[resource] == null)
            this.awaitingList[resource] = {};

        

        if(this.registry[resource] == token)
        {
            //Already reserved by this token, incrementing count
            ++this.tokenReservationCount[token];
            var result = "SemaphoreController: " + token + " reserved " + resource + " tokenCount: " + this.tokenReservationCount[token];
            
            this.Log(resource, result);
        }
        else
        {
            if(this.awaitingList[resource][token] == null)
            {
                this.awaitingList[resource][token] = (new Date()).getTime();
                this.awaitQueue[resource].push(token);
            }
            
            if(this.registry[resource] == null && this.awaitQueue[resource][0] == token)
            {
                this.awaitingList[resource][token] = null;
                this.awaitQueue[resource].shift();
                
                return this.ReserveToken(resource, token);                   
                
            }
            else
            {
                task.repeatDelay(this.RepeatDelayPeriod);
                return "SemaphoreController: " + token + " waiting on " + resource;;
            }
        }

        return result;
    }

    this.Release = function(resource)
    {
        var processId = plate.name + " " + plate.instance;
        
        return this.ReleaseToken(resource, processId);
    }
    
    this.ReleaseToken = function(resource, token)
    {
        var result = "";
        if(this.registry[resource] == token)
        {
            --this.tokenReservationCount[token];

            if(this.tokenReservationCount[token] == 0)
            {
                this.registry[resource] = null;

                result = "SemaphoreController: " + token + " releasing " + resource;
            }
            else
            {
                result = "SemaphoreController: " + token + " decrementing " + resource + " tokenCount: " + this.tokenReservationCount[token];
            }
        }
        else
        {
            throw "Exception: SemaphoreController: " + token + " tried to release " + resource + " for which it did not have a reservation.";
        }

        this.Log(resource, result);

        return result;
    }

    this.IsFree = function(resource)
    {
        return this.registry[resource] == null;
    }
    
    this.Reserve = function(resource)
    {
        var processId = plate.name + " " + plate.instance;
        return this.ReserveToken(resource, processId);
    }

    this.ReserveToken = function(resource, token)
    {
        this.registry[resource] = token;
        this.tokenReservationCount[token] = 1;

        var result = "SemaphoreController: " + token + " reserved " + resource + " tokenCount: " + this.tokenReservationCount[token];
        this.Log(resource, result);

        return result;
    }

    this.Log = function(resource, text)
    {            
        if(this.EnableLogging)
        {
            var file = new File();
            file.Open(this.filePath + " semaphore " + resource + ".log");
            file.Write("\n" + text);           
            file.Close();
        }
    }
}