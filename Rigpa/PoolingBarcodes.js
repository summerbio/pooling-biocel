function PoolingBarcodes()
{
	function Init()
	{
		try
		{
			if (WRITE_TO_LIMS)
			{
				// run LIMS Verification app to confirm connection with Mirth/LIMS
				run(BarcodeFolder + "\\LIMSVerification\\App\\run-LIMSVerificationApp.bat");
			}

			SimDiscreteRackBarcodeCounter = 0;
			SimPooledRackBarcodeCounter = 0;

			PoolingBarcodeObjects = {};

			jsonSerializer = new JSONSerializer();
		}
		catch(e)
		{
			Error("PoolingBarcodes.Init", "Exception", e);
		}
	}

	function LIMSOffWarning()
	{
		try
		{
			if (!WRITE_TO_LIMS)
			{
				task.Title = "LIMS COMMUNICATION TURNED OFF";
				task.Body = "If this is not intentional: abort the protocol and contact a support engineer.";
			}
			else
			{
				task.skip();
			}
		}
		catch(e)
		{
			Error("PoolingBarcodes.LIMSOffWarning", "Exception", e);
		}
	}

	function CreateSimPooledBarcode()
	{
		try
		{
			var simBarcode = "P";
			var simBarcodeNumber = String(SimPooledRackBarcodeCounter++);
			for(var si = 0; si < 7 - simBarcodeNumber.length; ++si)
            {
                simBarcode += "0";
            }
            simBarcode += simBarcodeNumber;
            plate.setBarcode(WEST, simBarcode);
		}
		catch(e)
		{
			Error("PoolingBarcodes.CreateSimPooledBarcode", "Exception", e);
		}
	}

	function CreateSimDiscreteBarcode()
	{
		try
		{
			var simBarcode = "D";
			var simBarcodeNumber = String(SimDiscreteRackBarcodeCounter++);
			for(var si = 0; si < 7 - simBarcodeNumber.length; ++si)
	        {
	            simBarcode += "0";
	        }
	        simBarcode += simBarcodeNumber;
	        plate.setBarcode(WEST, simBarcode);
		}
		catch(e)
		{
			Error("PoolingBarcodes.CreateSimDiscreteBarcode", "Exception", e);			
		}
	}

	function RegisterPooledRackStart()
	{
		try
		{
			var poolingGroupID = plate.instance;
			PoolingBarcodeObjects[poolingGroupID] = {};

			var p  = PoolingBarcodeObjects[poolingGroupID];

            p.PooledRackBarcode = plate.barcode[WEST];
            p.PooledRackStartTime = (new Date()).toString();
            p.PooledRackEndTime = null;
            p.DiscreteBarcodes = [];
			p.PoolingGroupID = poolingGroupID;
            p.SystemName = SystemName;
            p.DiscreteBarcodeCount = 0;
            p.DiscreteBarcodeCheckSum = 0;
            p.SubmittedToLIMS = false;

		}
		catch(e)
		{
			Error("PoolingBarcodes.RegisterPooledRackBarcode", "Exception", e);			
		}
	}

	function RegisterPooledRackEnd()
	{
		try
		{
			var poolingGroupID = plate.instance;

			var p  = PoolingBarcodeObjects[poolingGroupID];

            p.PooledRackEndTime = (new Date()).toString();

            writePoolingJSONFile();
            writePoolingFLGFile();
		}
		catch(e)
		{
			Error("PoolingBarcodes.RegisterPooledRackEnd", "Exception", e);			
		}
	}

	function RegisterDiscreteRackStart()
	{
		try
		{
			var poolingGroupID = Math.ceil(plate.instance / PoolingRatio);
			var discreteID = (plate.instance - 1) % PoolingRatio;

			var p  = PoolingBarcodeObjects[poolingGroupID];

			p.DiscreteBarcodeCount++;
			p.DiscreteBarcodes[discreteID] = {};
			var d = p.DiscreteBarcodes[discreteID];

			d.DiscreteBarcode = plate.barcode[WEST];
			d.DiscreteRackStartTime = (new Date()).toString();
		}
		catch(e)
		{
			Error("PoolingBarcodes.RegisterDiscreteRackBarcode", "Exception", e);			
		}
	}

	function RegisterDiscreteRackEnd()
	{
		try
		{
			var poolingGroupID = Math.ceil(plate.instance / PoolingRatio);
			var discreteID = (plate.instance - 1) % PoolingRatio;

			var p  = PoolingBarcodeObjects[poolingGroupID];

			var d = p.DiscreteBarcodes[discreteID];

			d.DiscreteRackEndTime = (new Date()).toString();
		}
		catch(e)
		{
			Error("PoolingBarcodes.RegisterDiscreteRackEnd", "Exception", e);			
		}
	}

	function writePoolingJSONFile()
	{
		try
		{
			var poolingGroupID = plate.instance;
			var p  = PoolingBarcodeObjects[poolingGroupID];

			if (p == null)
			{
				throw("Trying to write JSON file for a pooled group that doesn't exist!")
			}
			if (WRITE_TO_LIMS)
			{
				p.SubmittedToLIMS = true;
				FileOperations.OverwriteFile(BarcodeFolder + "pooling barcodes " + p.PooledRackBarcode + ".json", jsonSerializer.SerializeFormatted(p));
			}
		}
		catch(e)
		{
			Error("PoolingBarcodes.writePoolingJSONFile", "Exception", e);			
		}
	}

	function writePoolingFLGFile()
	{
		try
		{
			var poolingGroupID = plate.instance;
			var p  = PoolingBarcodeObjects[poolingGroupID];

			if (p == null)
			{
				throw("Trying to write FLG file for a pooled group that doesn't exist!")
			}

            if(WRITE_TO_LIMS)
            {
                FileOperations.OverwriteFile(BarcodeFolder + "pooling done " + p.PooledRackBarcode + ".flg", "Ready to process");
            }
		}
		catch(e)
		{
			Error("PoolingBarcodes.writePoolingFLGFile", "Exception", e);						
		}
	}
}