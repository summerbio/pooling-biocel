function Form()
{
	function Init()
	{
		try
		{
			FULL_SCREEN_ON = true;

			FormTextPooledRack = "";
			FormTextDiscreteRack = "";
			FormProgressPooledRack = 0;
			FormPoolingRatioText = "Pooling Ratio\n" + PoolingRatio + ":1";

			FormTextDispenseRack = "";
			FormTextTotalVolume = "";
			FormTextDispenseVolume_mL = "";
			FormTextDispenseVolume_uL = "";

			FormTextConveyor1 = "";
			FormTextConveyor2 = "";
			FormTextConveyor3 = "";
			FormTextConveyor4 = "";
			FormTextConveyor5 = "";
			FormTextConveyor6 = "";


			FormImagePath = "C:\\vworks workspace\\Form Images\\";
			BlankImagePath = FormImagePath + "1x1.png";
			FormImageDecapper1 = BlankImagePath;
			FormImageDecapper2 = BlankImagePath;
			FormImageDecapper3 = BlankImagePath;

			FormButtonRefreshDecapper1 = false;
			FormButtonRefreshDecapper2 = false;
			FormButtonRefreshDecapper3 = false;

			FormButtonClearDecapper1 = false;
			FormButtonClearDecapper2 = false;
			FormButtonClearDecapper3 = false;
		}
		catch(e)
		{
			Error("Form.Init", "Exception", e);
		}
	}

	function FullScreenOn()
	{
		try
		{
			if (FULL_SCREEN_ON)
			{
				forms.setFullScreen(true);
				task.repeatDelay(10);
			}
		}
		catch(e)
		{
			Error("Form.FullScreenOn", "Exception", e);
		}
	}

	// in preparation for the end of the protocol, turn off form full screen!
	function FullScreenOff()
	{
		try
		{
			FULL_SCREEN_ON = false;
		}
		catch(e)
		{
			Error("Form.FullScreenOff", "Exception", e);
		}
	}

	function UpdateFormDiscreteRackList()
	{
		try
		{
			if (FormTextDiscreteRack == "")
			{
				FormTextDiscreteRack = plate.barcode[WEST];
			}
			else
			{
				FormTextDiscreteRack = FormTextDiscreteRack + "\n" + plate.barcode[WEST];
			}
		}
		catch(e)
		{
			Error("Form.UpdateTextDiscreteRacks", "Exception", e);
		}
	}

	function UpdateFormProgressBar()
	{
		try
		{
			var discreteID = (plate.instance - 1) % PoolingRatio;
			
			// scaling to allow proper rendering on form
			FormProgressPooledRack = 100*((discreteID +1)/ PoolingRatio);
		}
		catch(e)
		{
			Error("Form.UpdateFormProgressBar", "Exception", e);
		}
	}	

	function UpdateFormPooledRack()
	{
		try
		{
			FormProgressPooledRack = 0;
			FormTextDiscreteRack = "";
			FormTextPooledRack = plate.barcode[WEST];
		}
		catch(e)
		{
			Error("Form.UpdateFormPooledRack", "Exception", e);
		}
	}

	function UpdateFormConveyorEmpty(conveyorID)
	{
		try
		{
			switch(conveyorID)
			{
				case 1:
					FormTextConveyor1 = "1: EMPTY";
					break;
				case 2:
					FormTextConveyor2 = "2: EMPTY";
					break;
				case 3:
					FormTextConveyor3 = "3: EMPTY";
					break;
				default:
					throw("conveyorID: " + conveyorID + " is not a valid input conveyor")
			}
		}
		catch(e)
		{
			Error("Form.UpdateFormConveyorEmpty", "Exception", e);
		}
	}

	function UpdateFormConveyorFull(conveyorID)
	{
		try
		{
			switch(conveyorID)
			{
				case 4:
					FormTextConveyor4 = "4: FULL";
					break;
				case 5:
					FormTextConveyor5 = "5: FULL";
					break;
				case 6:
					FormTextConveyor6 = "6: FULL";
					break;
				default:
					throw("conveyorID: " + conveyorID + " is not a valid output conveyor")
			}
		}
		catch(e)
		{
			Error("Form.UpdateFormConveyorFull", "Exception", e);
		}
	}

	function UpdateFormConveyorNominal(conveyorID)
	{
		try
		{
			switch(conveyorID)
			{
				case 1:
					FormTextConveyor1 = "";
					break;
				case 2:
					FormTextConveyor2 = "";
					break;
				case 3:
					FormTextConveyor3 = "";
					break;
				case 4:
					FormTextConveyor4 = "";
					break;
				case 5:
					FormTextConveyor5 = "";
					break;
				case 6:
					FormTextConveyor6 = "";
					break;
				default:
					throw("conveyorID: " + conveyorID + " is not a valid conveyor")
			}
		}
		catch(e)
		{
			Error("Form.UpdateFormConveyorNominal", "Exception", e);
		}
	}

	function UpdateFormVolumesPreDispense()
	{
		try
		{
			FormTextDispenseRack = plate.barcode[WEST],
			FormTextTotalVolume = String(TotalVolume.toFixed(4)) + " L";
			FormTextDispenseVolume_mL = "";
			FormTextDispenseVolume_uL = "";
		}
		catch(e)
		{
			Error("Form.UpdateFormVolumesPreDispense", "Exception",e);
		}
	}

	function UpdateFormVolumesPostDispense()
	{
		try
		{
			FormTextTotalVolume = String(TotalVolume.toFixed(4)) + " L";
			FormTextDispenseVolume_mL = String(DispenseVolume_mL.toFixed(1)) + " mL";
			FormTextDispenseVolume_uL = String((DispenseVolume_mL / 96 * 1000).toFixed(0)) + " uL";
		}
		catch(e)
		{
			Error("Form.UpdateFormVolumesPostDispense", "Exception",e);
		}
	}
}