function UniFiCamera()
{
	function Init()
	{
		try
		{
			CameraSettingsPath = "c:\\pooling-biocel-settings\\UniFiCamera\\";
			CameraSnapshotUtilityPath = "C:\\vworks workspace\\Utilities\\UniFiCameraSnapshot.exe";
			ArchiveImagePath = "C:\\vworks workspace\\Archive Images\\";

			CameraDecapper1 = new this.UniFiCameraSnapshot("decapper1", CameraSettingsPath, CameraSnapshotUtilityPath);
			CameraDecapper2 = new this.UniFiCameraSnapshot("decapper2", CameraSettingsPath, CameraSnapshotUtilityPath);
			CameraDecapper3 = new this.UniFiCameraSnapshot("decapper3", CameraSettingsPath, CameraSnapshotUtilityPath);
		}
		catch(e)
		{
			Error("UniFiCamera.Init", "Exception", e);
		}
	}

	function UniFiCameraSnapshot(settingsFileName, settingsFolder, captureUtilityFilePath)
	{
		this.CaptureUtilityFilePath = captureUtilityFilePath;
		this.SettingsFileName = settingsFileName

		var file = new File();
		file.Open(settingsFolder + settingsFileName + ".json");
		var contents = file.Read();
		file.Close();

		eval("var unifiSettings = " + contents);

		this.Settings = unifiSettings;

		this.TakeSnapshot = function(rawFilePath, croppedFilePath)
		{
			var runString = "\"" + this.CaptureUtilityFilePath + "\" \"" + this.Settings.IPAddress + "\" \"" + rawFilePath + "\" \"" + croppedFilePath + "\" " + this.Settings.CropX + " " + this.Settings.CropY + " " + this.Settings.Width + " " + this.Settings.Height + " " + this.Settings.ResultWidth + " " + this.Settings.Rotation;
			run(runString, true);
			return(runString);
		}
	}
}

