function FileOperations()
{
    function AppendFile(filePath, newline)
    {
    	try
    	{
	        var file = new File();

	        file.Open(filePath);

	        file.Write(newline);
	        file.Close();
    	}
	    catch(e)
	    {
	        Error("FileOperations.AppendFile", "Exception", e);
	    }

    }

    function ReadFileContents(filePath)
	{
	    try
	    {
	        var fileToRead = new File();
	        fileToRead.Open(filePath);
	        var fileToReadContents = fileToRead.Read();
	        fileToRead.Close();

	        return fileToReadContents;
	    }
	    catch(e)
	    {
	        Error("FileOperations.ReadFileContents", "Exception", e);
	    }
	}

	function OverwriteFile(filePath, contents)
	{
		try
		{
		    var file = new File();

		    file.Open(filePath, 'w');
		    file.Write(contents);
		    file.Close();
		}
	    catch(e)
	    {
	        Error("FileOperations.OverwriteFile", "Exception", e);
	    }
	}
}