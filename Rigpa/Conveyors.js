function Conveyors()
{

	function getToken(conveyorID)
	{
		try
		{
			var token;
			switch(conveyorID)
			{
				case 1:
					token = "Discrete Rack In";
					break;
				case 2:
					token = "Pooled Rack In";
					break;
				case 3:
					token = "Tip Box In";
					break;
				case 4:
					token = "Discrete Rack Out";
					break;
				case 5:
					token = "Pooled Rack Out";
					break;
				case 6:
					token = "Tip Box Out";
					break;
				default:
					throw("conveyorID: " + conveyorID + " is not a valid output conveyor")
			}
			token = token + " " + plate.instance;
			return token;		
		}
        catch(e)
        {
             Error("Conveyors.getToken", "Exception", e);
        }
	}

	function Reserve(conveyorID)
	{
		try
		{
			var resource = "Conveyor-" + conveyorID;
			var token = getToken(conveyorID);
			PoolingSemaphoreController.AwaitToken(resource, token)
		}
        catch(e)
        {
             Error("Conveyors.Reserve", "Exception", e);
        }
	}
	function Release(conveyorID)
	{
		try
		{
			var resource = "Conveyor-" + conveyorID;
			var token = getToken(conveyorID);
			PoolingSemaphoreController.ReleaseToken(resource, token)
		}
        catch(e)
        {
             Error("Conveyors.Release", "Exception", e);
        }
	}
}