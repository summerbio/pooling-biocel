function TubeBarcodeCheckerTasks()
{
    function Initialize()
    {        
        try
        {
        	if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}
            TubeBarcodeChecker.resetState();
            TubeBarcodeChecker.setRackBarcode(String(plate.barcode[WEST]));
            TubeBarcodeChecker.findLatestFile();            
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.Initialize", "Exception", e);
        }
    }

    function ScanDiscreteRack()
    {
        try
        {
            if (!WRITE_TO_LIMS)
            {
                task.skip();
            }

			if (SKIP_USER_PROMPTS)
			{
	        	task.Validatenumberoftubes = false;
			}

            task.DefaultOutputfilename = BarcodeFolder + "rack_" + plate.barcode[WEST] + ".csv";
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.ScanDiscreteRack", "Exception", e);
        }
    }

    function ScanPooledRack()
    {
        try
        {
            if (!WRITE_TO_LIMS)
            {
                task.skip();
            }
            
			if (SKIP_USER_PROMPTS)
			{
	        	task.Validatenumberoftubes = false;
			}

            task.DefaultOutputfilename = BarcodeFolder + "rack_" + plate.barcode[WEST] + ".csv";
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.ScanPooledRack", "Exception", e);
        }
    }

    function OuterLoopBegin()
    {        
        try
        {
        	if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}

            // reset this flag, which controls inner loop!
            TubeBarcodeChecker.badInputFlag = null;
            
            if (TubeBarcodeChecker.fileName == null) // final pass of outer loop
            {
                task.skip();
            }
            else if (TubeBarcodeChecker.badBarcodeFlag == false) // final pass of outer loop
            {
                task.skip();
            }
            else if (TubeBarcodeChecker.badBarcodeFlag == null) // first pass of outer loop
            {

            }
            else if (TubeBarcodeChecker.badBarcodeFlag == true) // secondary passes of outer loop
            {

            }      
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.OuterLoopBegin", "Exception", e);
        }
    }

    function FirstPrompt()
    {        
        try
        {
			if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}

            // final pass of outer loop
            if (TubeBarcodeChecker.fileName == null) {task.skip()}

            // final pass of outer loop
            else if (TubeBarcodeChecker.badBarcodeFlag == false) {task.skip()}

            // first or secondary passes of outer loop
            else if (TubeBarcodeChecker.badBarcodeFlag == null || TubeBarcodeChecker.badBarcodeFlag == true) {
                TubeBarcodeChecker.readFile()

                if (TubeBarcodeChecker.badBarcodeFlag == true) {
                    task.Title = TubeBarcodeChecker.titleText
                    TubeBarcodeChecker.resetTubeBarcode()
                    newBarcode = null // declare and initialize this variable!
                }
                else {task.skip()}
            }
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.FirstPrompt", "Exception", e);
        }
    }

    function InnerLoopBegin()
    {        
        try
        {
			if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}

            // final pass of outer loop
            if (TubeBarcodeChecker.badBarcodeFlag != true) {task.skip()}
            
            // final pass of inner loop
            else if (TubeBarcodeChecker.badInputFlag == false) {task.skip()}
            
            // first pass of inner loop
            else if (TubeBarcodeChecker.badInputFlag == null) {}
            
            // secondary passes of inner loop
            else if (TubeBarcodeChecker.badInputFlag == true) {}
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.InnerLoopBegin", "Exception", e);
        }
    }

    function SecondPrompt()
    {        
        try
        {
			if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}

            // final pass of outer loop
            if (TubeBarcodeChecker.badBarcodeFlag != true) {task.skip()} 

            // final pass of inner loop
            else if (TubeBarcodeChecker.badInputFlag == false) {task.skip()}

            //first and secondary passes of inner loop
            else if (TubeBarcodeChecker.badInputFlag == null || TubeBarcodeChecker.badInputFlag == true) {

            TubeBarcodeChecker.setTubeBarcode(String(newBarcode))
            TubeBarcodeChecker.checkInput()

            if(TubeBarcodeChecker.badInputFlag == true) {
                task.Title = TubeBarcodeChecker.titleText
                TubeBarcodeChecker.resetTubeBarcode()
                newBarcode = null // declare and initialize this variable!
            }
            else {task.skip()}
            }

        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.SecondPrompt", "Exception", e);
        }
    } 
    
    
    function WriteNewFile()
    {        
        try
        {
			if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}

            if (TubeBarcodeChecker.badBarcodeFlag == true) {
                TubeBarcodeChecker.setTubeBarcode(String(newBarcode))
                TubeBarcodeChecker.writeFile()
            }
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.WriteNewFile", "Exception", e);
        }
    }
    
    function ThirdPrompt()
    {        
        try
        {
        	if (SKIP_USER_PROMPTS)
			{
				task.skip();
			}

            if (TubeBarcodeChecker.badBarcodeFlag != true) {
                task.skip()
            }
        }
        catch(e)
        {
            Error("TubeBarcodeCheckerTasks.ThirdPrompt", "Exception", e);
        }
    }
}