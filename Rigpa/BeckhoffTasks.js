function BeckhoffTasks()
{
	function BeckhoffTask()
	{
		try
		{
			if (task.isSimulatorRunning())
			{
				task.skip();
			}
		}
		catch(e)
        {
            Error("BeckhoffTasks.BeckhoffTask", "Exception", e);
        }
	}

	function WaitForInputConveyor(conveyorID)
	{
		try
		{
			Form.UpdateFormConveyorEmpty(conveyorID);
		}
		catch(e)
        {
            Error("BeckhoffTasks.WaitForInputConveyor", "Exception", e);
        }
	}

	function DoneWaitForInputConveyor(conveyorID)
	{
		try
		{
			Form.UpdateFormConveyorNominal(conveyorID);
		}
		catch(e)
        {
            Error("BeckhoffTasks.DoneWaitForInputConveyor", "Exception", e);
        }
	}

	function WaitForOutputConveyor(conveyorID)
	{
		try
		{
			Form.UpdateFormConveyorFull(conveyorID);
		}
		catch(e)
        {
            Error("BeckhoffTasks.WaitForOutputConveyor", "Exception", e);
        }
	}

	function DoneWaitForOutputConveyor(conveyorID)
	{
		try
		{
			Form.UpdateFormConveyorNominal(conveyorID);
		}
		catch(e)
        {
            Error("BeckhoffTasks.DoneWaitForOutputConveyor", "Exception", e);
        }
	}

}