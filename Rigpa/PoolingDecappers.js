function PoolingDecappers()
{
    function Init()
    {
        try
        {
            // all decappers that are on system in nominal configuration
            AllDecappers = ["1Decapper-1", "1Decapper-2", "1Decapper-3"];

            // all decappers that are on system, excluding broken devices
            WorkingDecappers = getWorkingDecappers();
            DecapperResourcePool = new ResourcePool(WorkingDecappers, PoolingSemaphoreController, 2, LOG_PLATE_CONTROL);

            // swimlane name that's critical for reserving decappers for the cap carrier scheme
            PooledRackName = "Pooled Rack";

            DecapperChecker1 = new DecapperChecker(1);
            DecapperChecker2 = new DecapperChecker(2);
            DecapperChecker3 = new DecapperChecker(3);
        }
        catch(e)
        {
             Error("PoolingDecappers.Init", "Exception", e);
        }
    }

    function getWorkingDecappers()
    {
        var workingDecappers = [];
        if (! OMIT_DECAPPER_1)
        {
            workingDecappers.push(AllDecappers[0]);
        }
        if (! OMIT_DECAPPER_2)
        {
            workingDecappers.push(AllDecappers[1]);
        }
        if (! OMIT_DECAPPER_3)
        {
            workingDecappers.push(AllDecappers[2]);
        }
        return workingDecappers;
    }


    function DecapperCheckerTask()
    {
        try
        {
            if (SKIP_USER_PROMPTS)
            {
                task.skip();
            }
            
            var decapper = DecapperResourcePool.Reservation();
            switch(decapper)
            {
                case(AllDecappers[0]):
                    DecapperChecker1.Update();
                    break;
                case(AllDecappers[1]):
                    DecapperChecker2.Update();
                    break;
                case(AllDecappers[2]):
                    DecapperChecker3.Update();
                    break;
                default:
                    throw("decapperID: " + decapper + " is not a valid decapper");
            }

        }
        catch(e)
        {
            Error("PoolingDecappers.DecapperCheckerTask", "Exception", e);
        }
    } 

    function DecapperCheckerCapCarrierTask()
    {
        try
        {
            if (SKIP_USER_PROMPTS)
            {
                task.skip();
            }

            var decapper = DecapperResourcePool.Registry[CapCarrierEndToken];
            switch(decapper)
            {
                case(AllDecappers[0]):
                    DecapperChecker1.Update();
                    break;
                case(AllDecappers[1]):
                    DecapperChecker2.Update();
                    break;
                case(AllDecappers[2]):
                    DecapperChecker3.Update();
                    break;
                default:
                    throw("decapperID: " + decapper + " is not a valid decapper");
            }

        }
        catch(e)
        {
            Error("PoolingDecappers.DecapperCheckerCapCarrierTask", "Exception", e);
        }
    }  

    function DisabledDecapperWarning()
    {
        try
        {
            if (OMIT_DECAPPER_1 && OMIT_DECAPPER_2 && OMIT_DECAPPER_3)
            {
                task.Title = "ALL DECAPPERS DISABLED";
                task.Body = "At least one decapper must be enabled. Abort the protocol, change the setting in the Form, and try again."
                return true;
            }
            if (OMIT_DECAPPER_1 && OMIT_DECAPPER_2)
            {
                task.Title = "1Decapper-1 AND 1Decapper-2 DISABLED";
                task.Body = "If this is not intentional: abort the protocol, change the setting in the Form, and try again. Otherwise, continue.";
                return true;
            }
            if (OMIT_DECAPPER_2 && OMIT_DECAPPER_3)
            {
                task.Title = "1Decapper-2 AND 1Decapper-3 DISABLED";
                task.Body = "If this is not intentional: abort the protocol, change the setting in the Form, and try again. Otherwise, continue.";
                return true;
            }
            if (OMIT_DECAPPER_1 && OMIT_DECAPPER_3)
            {
                task.Title = "1Decapper-1 AND 1Decapper-3 DISABLED";
                task.Body = "If this is not intentional: abort the protocol, change the setting in the Form, and try again. Otherwise, continue.";
                return true;
            }
            if (OMIT_DECAPPER_1)
            {
                task.Title = "1Decapper-1 DISABLED";
                task.Body = "If this is not intentional: abort the protocol, change the setting in the Form, and try again. Otherwise, continue.";
                return true;
            }
            if (OMIT_DECAPPER_2)
            {
                task.Title = "1Decapper-2 DISABLED";
                task.Body = "If this is not intentional: abort the protocol, change the setting in the Form, and try again. Otherwise, continue.";
                return true;
            }
            if (OMIT_DECAPPER_3)
            {
                task.Title = "1Decapper-3 DISABLED";
                task.Body = "If this is not intentional: abort the protocol, change the setting in the Form, and try again. Otherwise, continue.";
                return true;
            }
            // if all decappers are enabled!
            task.skip();
            return true;
        }
        catch(e)
        {
            Error("PoolingDecappers.DisabledWarningMessage", "Exception", e);
        }
    }

    function WaitForDecapper()
    {
        try
        {
            return DecapperResourcePool.AwaitResource();
        }
        catch(e)
        {
            Error("PoolingDecappers.WaitForDecapper", "Exception", e);
        }
    }

    function ReleaseDecapper()
    {
        try
        {
            return DecapperResourcePool.Release();
        }
        catch(e)
        {
            Error("PoolingDecappers.ReleaseDecapper", "Exception", e);
        }
    }

    function DecapTask(decapperIndex)
    {
        try
        {
            // shift decapper index when referencing zero-indexed array
            decapperName = AllDecappers[decapperIndex - 1];
            if(DecapperResourcePool.Reservation() != decapperName)
            {
                task.skip();
                return "Skipping Decap " + decapperName;
            }
            else
            {
                print(plate.barcode[WEST] + " decapping at " + decapperName);
                return "Decapping " + decapperName;
            }
        }
        catch(e)
        {
            Error("PoolingDecappers.DecapTask", "Exception", e);
        }
    }

    function RecapTask(decapperIndex)
    {
        try
        {
            // shift decapper index when referencing zero-indexed array
            decapperName = AllDecappers[decapperIndex - 1];
            if(DecapperResourcePool.Reservation() != decapperName)
            {
                task.skip();
                return "Skipping Recap " + decapperName;
            }
            else
            {
                print(plate.barcode[WEST] + " recapping at " + decapperName);
                return "Recapping " + decapperName;
            }
        }
        catch(e)
        {
            Error("PoolingDecappers.RecapTask", "Exception", e);
        }
    }

    function FirstPooledRecapTask(decapperIndex)
    {
        try
        {
            // cap carrier always holds caps from the first pooled rack
            processId = PooledRackName + " 1";

            // shift decapper index when referencing zero-indexed array
            decapperName = AllDecappers[decapperIndex - 1];
            if(DecapperResourcePool.Registry[processId] != decapperName)
            {
                task.skip();
                return "Skipping Unload Pooled Caps " + decapperName;
            }
            else
            {
                print("CAP CARRIER recapping at "+ decapperName);
                return "Unloading Pooled Caps " + decapperName;
            }
        }
        catch(e)
        {
            Error("PoolingDecappers.FirstPooledRecapTask", "Exception", e);
        }
    }

    function PooledRecapTask(decapperIndex)
    {
        try
        {
            nextInstance = plate.instance + 1;
            processId = plate.name + " " + nextInstance;
            decapperName = AllDecappers[decapperIndex - 1];
            if(DecapperResourcePool.Registry[processId] != decapperName)
            {
                task.skip();
                return "Skipping Unload Pooled Caps " + decapperName;
            }
            else
            {
                print(plate.barcode[WEST] + " recapping at " + decapperName);
                return "Unloading Pooled Caps " + decapperName;
            }
        }
        catch(e)
        {
            Error("PoolingDecappers.PooledRecapTask", "Exception", e);
        }
    }

    function FirstPooledReleaseResourcePool()
    {
        try
        {
            processId =  PooledRackName + " 1";
            return DecapperResourcePool.ReleaseToken(processId);
        }
        catch(e)
        {
            Error("PoolingDecappers.PooledReleaseResourcePool", "Exception", e);
        }
    }

    function PooledReleaseResourcePool()
    {
        try
        {
            nextInstance = plate.instance + 1;
            processId = plate.name + " " + nextInstance;
            return DecapperResourcePool.ReleaseToken(processId);
        }
        catch(e)
        {
            Error("PoolingDecappers.PooledReleaseResourcePool", "Exception", e);
        }
    }

    function DecappingMessageBox()
    {        
        try
        {
            if (SKIP_USER_PROMPTS)
            {
                task.skip();
            }
                 
            task.Title = "Check Decapping " + DecapperResourcePool.Reservation();
            task.Body = "Did decapping succeed on " + DecapperResourcePool.Reservation() + "?";

            return "";
        }
        catch(e)
        {
            Error("PoolingDecappers.DecappingMessageBox", "Exception", e);
        }
    }

    function SetCapCarrierEndToken()
    {
        try
        {
            CapCarrierEndToken = "Pooled Rack " + plate.instance;
        }
        catch(e)
        {
            Error("PoolingDecappers.SetTokenForCapCarrierEnd", "Exception", e);
        }
    }

    function WaitForDecapperCapCarrier()
    {
        try
        {
            CapCarrierEndToken = "Pooled Rack " + (PooledRacks.HighestInstance + 1);
            return DecapperResourcePool.AwaitResourceToken(CapCarrierEndToken);
        }
        catch(e)
        {
            Error("PoolingDecappers.WaitForDecapperCapCarrierEnd", "Exception", e);
        }
    }   

    function DecapTaskCapCarrier(decapperIndex)
    {
        try
        {
            // shift decapper index when referencing zero-indexed array
            decapperName = AllDecappers[decapperIndex - 1];
            if(DecapperResourcePool.Registry[CapCarrierEndToken] != decapperName)
            {
                task.skip();
                return "Skipping Decap " + decapperName;
            }
            else
            {
                print("CAP CARRIER decapping at " + decapperName);
                return "Decapping " + decapperName;
            }
        }
            catch(e)
        {
            Error("PoolingDecappers.WaitForDecapperCapCarrierEnd", "Exception", e);
        }
    }
}