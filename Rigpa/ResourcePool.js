function ResourcePool(resourceArray, semaphoreController, repeatDelayPeriod, enableLogging)
{
    this.ResourceArray = resourceArray;
    this.SemaphoreController = semaphoreController;
    this.Registry = {};
    this.awaitQueue = [];
    this.awaitingList = {};
    this.EnableLogging = enableLogging;
    this.RepeatDelayPeriod = repeatDelayPeriod;

    this.AwaitResourceToken = function(processId)
    {            
        if(this.awaitingList[processId] == null)
        {
            this.awaitingList[processId] = (new Date()).getTime();
            this.awaitQueue.push(processId);
        }

        var resource = null;
        
        if(this.awaitQueue[0] == processId)
        {
            var i = 0;
            while(resource == null && i < resourceArray.length)
            {
                if(this.SemaphoreController.IsFree(this.ResourceArray[i]))
                {
                    this.awaitQueue.shift();
                    this.awaitingList[processId] = null;

                    var result = this.SemaphoreController.ReserveToken(this.ResourceArray[i], processId);

                    if(this.EnableLogging)
                        print(result);

                    this.Registry[processId] = this.ResourceArray[i];
                    
                    if(this.EnableLogging)
                        print("ResourcePool: " + processId + " reserved " + this.Registry[processId]);

                    return this.ResourceArray[i];
                }

                ++i;
            }
        }

        if(resource == null)
        {
            task.repeatDelay(this.RepeatDelayPeriod);
            
            if(this.EnableLogging)
                print("ResourcePool: " + processId + " waiting on " + resourceArray);
        }
    }

    this.AwaitResource = function()
    {
        var processId = plate.name + " " + plate.instance;
        
        this.AwaitResourceToken(processId);
    }

    this.ReleaseToken = function(processId)
    {            
        if(this.Registry[processId] != null)
        {                
            var result = this.SemaphoreController.ReleaseToken(this.Registry[processId], processId);
            
            if(this.EnableLogging)
            {
                print(result);                
                print("ResourcePool: " + processId + " releasing " + this.Registry[processId]);
            }

            this.Registry[processId] = null;
        }
        else
        {
            throw "ResourcePool: " + processId + " tried to release but had no reservations.";
        }
    }

    this.Release = function()
    {
        var processId = plate.name + " " + plate.instance;
        
        return this.ReleaseToken(processId);
    }

    this.Reservation = function()
    {        
        var processId = plate.name + " " + plate.instance;

        return this.Registry[processId];
    }
}