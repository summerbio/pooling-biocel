function PoolingFlowControl()
{
	function Init()
	{
		try
		{
			// object for tracking pooled rack data in the protocol
			PooledRacks = {};

			// highest instance pooled rack on the system
			PooledRacks.HighestInstance = 0;

			// pooled racks instances will be added in this array until they are filled with sample
			// from all constituent discrete racks
			// this is used to track when to release the cap carrier into the system
			PooledRacks.InProcess = [];
		}
		catch(e)
		{
			Error("PoolingFlowControl.Init", "Exception", e);
		}
	}

	function PooledRackStart()
	{
		try
		{
			PoolingLogs.UpdateLog(PooledRackStartLog);

			PooledRacks.HighestInstance = plate.instance;
			PooledRacks.InProcess.unshift(plate.instance);
		}
		catch(e)
		{
            Error("PoolingFlowControl.PooledRackStart", "Exception", e);
		}
	}

	function PooledRackScanned()
	{
		try
		{
			PoolingBarcodes.RegisterPooledRackStart();
		}
		catch(e)
		{
            Error("PoolingFlowControl.PooledRackScanned", "Exception", e);
		}
	}

	function PooledRackOnVPrep()
	{
		try
		{
			Form.UpdateFormPooledRack();
		}
		catch(e)
		{
            Error("PoolingFlowControl.PooledRackOnVPrep", "Exception", e);
		}
	}
	function PooledRackFilledWithSample()
	{
		try
		{
			// once a pooled rack has all of it's constituent discrete racks, it's "done" being processed, 
			// therefore remove it from the InProcess array
			PooledRacks.InProcess.shift();
		}
		catch(e)
		{
			Error("PoolingFlowControl.PooledRackFilledWithSample", "Exception", e);
		}
	}

	function PooledRackEnd()
	{
		try
		{
			PoolingBarcodes.RegisterPooledRackEnd();
			PoolingLogs.UpdateLog(PooledRackEndLog);
		}
		catch(e)
		{
			Error("PoolingFlowControl.PooledRackEnd", "Exception", e);
		}
	}

	function DiscreteRackStart()
	{
		try
		{
			PoolingLogs.UpdateLog(DiscreteRackStartLog);
		}
		catch(e)
		{
			Error("PoolingFlowControl.DiscreteRackStart", "Exception", e);
		}
	}

	function DiscreteRackScanned()
	{
		try
		{
			PoolingBarcodes.RegisterDiscreteRackStart();
		}
		catch(e)
		{
            Error("PoolingFlowControl.RegisterDiscreteRackStart", "Exception", e);
		}
	}

	function DiscreteRackExtracted()
	{
		try
		{
			Form.UpdateFormDiscreteRackList();
			Form.UpdateFormProgressBar();
		}
		catch(e)
		{
            Error("PoolingFlowControl.DiscreteRackExtracted", "Exception", e);
		}
	}

	function DiscreteRackEnd()
	{
		try
		{
			PoolingBarcodes.RegisterDiscreteRackEnd();
			PoolingLogs.UpdateLog(DiscreteRackEndLog);
		}
		catch(e)
		{
			Error("PoolingFlowControl.DiscreteRackEnd", "Exception", e);
		}
	}

	function SpawnReleasePooledRackForRecapping()
	{
		try
		{
			// only skip this when it's run the first time, after that all Pooled +1 Racks should give their caps to the previous Pooled Rack
			if (plate.instance == 1) 
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingFlowControl.SpawnReleasePooledRackForRecapping", "Exception", e);
		}
	}

	function SpawnPooledRack()
	{
		try
		{
			// spawn the next pooled rack after the last discrete rack of a set gets a decapper 
			// and if the user has not indicated that they're done pooling
			if ((plate.instance % PoolingRatio == 0)  && !DONE_POOLING)
			{
				// spawn!
			}
			else
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingFlowControl.SpawnPooledRack", "Exception", e);
		}
	}

	function LoopStartSpawnDiscreteRack()
	{
		try
		{
			task.Numberoftimestoloop = PoolingRatio;
		}
		catch(e)
		{
            Error("PoolingFlowControl.LoopStartSpawnDiscreteRack", "Exception", e);
		}
	}

	function LoopStartPipetting()
	{
		try
		{
			task.Numberoftimestoloop = PoolingRatio;
		}
		catch(e)
		{
            Error("PoolingFlowControl.LoopStartPipetting", "Exception", e);
		}
	}

	function SpawnNotCapCarrier()
	{
		try
		{
			if (RackBarcodeChecker.isValidCapCarrierRackBarcode())
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingFlowControl.SpawnNotCapCarrier", "Exception", e);
		}	
	}

	function SpawnCapCarrier()
	{
		try
		{
			if(plate.instance != 1)
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingFlowControl.SpawnCapCarrier", "Exception", e);
		}	
	}

	function SpawnUnloadFirstCaps()
	{
		try
		{
			if (plate.instance != 1)
			{
				task.skip();
			}
		}
		catch(e)
		{
            Error("PoolingFlowControl.SpawnUnloadFirstCaps", "Exception", e);
		}	
	}

	// don't release another pooled rack into the system if the operator has signalled that they're done pooling
	function HoldPooledRack()
	{
		try
		{
			if (DONE_POOLING)
			{
				task.repeatDelay(10);
			}
		}
		catch(e)
		{
            Error("PoolingFlowControl.HoldPooledRack", "Exception", e);
		}
	}
}
