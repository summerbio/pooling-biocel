function PoolingPipetting()
{
	function Init()
	{
		try
		{
			// global object to store data
			PoolingPipettingParameters = {};

			// 340 uL required for first run of pooled test on an SB
			// 60 uL of dead volune (conservative)
			var pooledVolume = 400; // [uL]

			// low enough to reach liquid for low volume (450 uL) sample + vRB in tubes
			// high enough to avoid overflowing high volume (1000 uL) sample + vRB in tubes
			var startHeight = 10; // [mm]
			
			// used for miniscus-tracking during aspiration
			var dynamicTipExtension = 0.03; // [mm/uL]

			if (PoolingRatio != 2)
			{
				var discreteVolume = pooledVolume / PoolingRatio;
			}
			else
			{
				// special condition for PoolingRatio=2 to avoid having to do two aspirates
				var discreteVolume = 170;
			}
			
			// this is used for the "Distancefromwellbottom" parameter in an aspirate task
			var endHeight = startHeight - (dynamicTipExtension * discreteVolume);

			PoolingPipettingParameters.DynamicTipExtension = dynamicTipExtension;
			PoolingPipettingParameters.DiscreteVolume = discreteVolume;
			PoolingPipettingParameters.EndHeight = endHeight;
		}
		catch(e)
		{
            Error("PoolingPipetting.Init", "Exception", e);
		}
	}
	function FirstAspirateTask()
	{
		try
		{
			task.Dynamictipextension = PoolingPipettingParameters.DynamicTipExtension;
			task.Volume = PoolingPipettingParameters.DiscreteVolume; 
			task.Distancefromwellbottom = PoolingPipettingParameters.EndHeight;
		}
		catch(e)
        {
            Error("PoolingPipetting.FirstAspirateTask", "Exception", e);
        }
	}
}