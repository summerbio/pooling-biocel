function DecapperChecker(decapperID)
{
    this.DecapperID = decapperID;
    this.RefreshImage = false;
    this.AllClear = false;
    this.FirstPass = true;

    this.Update = function(refreshImage, allClear)
    {
        if (this.FirstPass)
        {
            this.clearImage();
            this.getImage();    
            this.resetFormVars();                     
            this.FirstPass = false;
            task.repeatDelay(1);
        }
        else
        {
            this.updateFormVars();
            if (this.AllClear)
            {
                this.clearImage();
                this.FirstPass = true; // prepare for next time!
            }
            else if (this.RefreshImage)
            {
                this.clearImage();
                this.getImage();
                this.resetFormVars();
                task.repeatDelay(1);
            }
            else
            {
                task.repeatDelay(1);
            }                    
        }
    }

    this.clearImage = function()
    {
        switch(decapperID)
        {
            case(1):
                FormImageDecapper1 = BlankImagePath;
                break;
            case(2):
                FormImageDecapper2 = BlankImagePath;
                break;
            case(3):
                FormImageDecapper3 = BlankImagePath;
                break;
            default:
                throw("decapperID: " + decapperID + " is not a valid decapper");
        }
    }

    this.getImage = function()
    {
        var barcode = plate.barcode[WEST];
        var timestamp = (new Date()).toString();
        timestamp = timestamp.replace("GMT-0700 (Pacific Daylight Time)","");
        timestamp = timestamp.replace(":","_");
        timestamp = timestamp.replace(":","_");

        switch(decapperID)
        {
            case(1):
                CameraDecapper1.TakeSnapshot(ArchiveImagePath + barcode + " " + timestamp + " Decapper1.jpg", FormImagePath + "Decapper1.jpg");
                FormImageDecapper1 = FormImagePath + "Decapper1.jpg";
                break;
            case(2):
                CameraDecapper2.TakeSnapshot(ArchiveImagePath + barcode + " " + timestamp + " Decapper2.jpg", FormImagePath + "Decapper2.jpg");
                FormImageDecapper2 = FormImagePath + "Decapper2.jpg";
                break;
            case(3):
                CameraDecapper3.TakeSnapshot(ArchiveImagePath + barcode + " " + timestamp + " Decapper3.jpg", FormImagePath + "Decapper3.jpg");
                FormImageDecapper3 = FormImagePath + "Decapper3.jpg";
                break;
            default:
                throw("decapperID: " + decapperID + " is not a valid decapper");
        }
    }

    this.resetFormVars = function()
    {
        switch(this.DecapperID)
        {
            case(1):
                FormButtonRefreshDecapper1 = false;
                FormButtonClearDecapper1 = false;
                break;
            case(2):
                FormButtonRefreshDecapper2 = false;
                FormButtonClearDecapper2 = false;
                break;
            case(3):
                FormButtonRefreshDecapper3 = false;
                FormButtonClearDecapper3 = false;
                break;
            default:
                throw("decapperID: " + decapperID + " is not a valid decapper");
        }   
    }

    this.updateFormVars = function()
    {
        switch(this.DecapperID)
        {
            case(1):
                this.RefreshImage = FormButtonRefreshDecapper1;
                this.AllClear = FormButtonClearDecapper1;
                break;
            case(2):
                this.RefreshImage = FormButtonRefreshDecapper2;
                this.AllClear = FormButtonClearDecapper2;
                break;
            case(3):
                this.RefreshImage = FormButtonRefreshDecapper3;
                this.AllClear = FormButtonClearDecapper3;
                break;
            default:
                throw("decapperID: " + decapperID + " is not a valid decapper");
        } 
    }
}