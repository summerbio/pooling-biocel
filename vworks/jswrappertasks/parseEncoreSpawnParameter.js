///<?xml version='1.0' encoding='ASCII' ?>
///<Velocity11 file='MetaData' md5sum='30e0aedc9f717663755171f8f9456527' version='1.0' >
///	<Command Compiler='0' Description='Parse Encore spawn parameter' Editor='-1' Name='Parse Encore spawn parameter' NextTaskToExecute='1' RequiresRefresh='0' TaskRequiresLocation='0' VisibleAvailability='1' >
///	  <Parameters >
///	  </Parameters>
///	</Command>
///</Velocity11>

function parseEncoreSpawnParameter(fileName, overWrite)
{
    var spawnParameters = plate.spawnParameter.split(",");
    for (nSpawn = 0; nSpawn < spawnParameters.length; ++nSpawn)
    {
        var nFind = spawnParameters[nSpawn].indexOf(":");
        if (nFind != -1)
        {
            var spawnName = spawnParameters[nSpawn].substring(0, nFind);
            var spaenValue = spawnParameters[nSpawn].substring(nFind+1);
            plate.setUserData(spawnName, spaenValue);
        }
    }
}
