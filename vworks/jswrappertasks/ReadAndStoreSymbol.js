///<?xml version='1.0' encoding='ASCII' ?>
///<Velocity11 file='MetaData' md5sum='30e0aedc9f717663755171f8f9456527' version='1.0' >
///	<Command Compiler='0' Description='JS Read And Store Symbol' Editor='-1' Name='JS Read And Store Symbol' NextTaskToExecute='1' RequiresRefresh='0' TaskRequiresLocation='0' VisibleAvailability='1' >
///	  <Parameters >
///		<Parameter Name='To array' Description='To array' Category='Array' Type='0' Value='0'/>
///		<Parameter Name='Array name' Description='Array name' Category='Array' Style='0' Scriptable='0' Type='1' Hide_if='Variable(To array) == Const(0)' />
///		<Parameter Name='Array index' Description='Array index' Category='Array' Type='3' Scriptable='1' Hide_if='Variable(To array) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Information' Description='Information' Category='Array' Type='3' Hide_if='Variable(To array) == Const(0)'>
///			<Ranges>
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='To file' Description='To file' Category='File' Type='0' Value='0'/>
///		<Parameter Name='File name' Description='File name' Category='File' ValueToDisplay='data file type(*.csv)|*.csv|' Type='9' Hide_if='Variable(To file) == Const(0)' />
///		<Parameter Name='Append' Description='Append' Category='File' Type='0' Hide_if='Variable(To file) == Const(0)' />
///		<Parameter Name='Separator' Description='Separator' Category='File' Type='2' Scriptable='0' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value=',' />
///				<Range Value='TAB' />
///				<Range Value=';' />
///				<Range Value=':' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 1' Description='Column 1' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 2' Description='Column 2' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 3' Description='Column 3' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 4' Description='Column 4' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 5' Description='Column 5' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 6' Description='Column 6' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 7' Description='Column 7' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///		<Parameter Name='Column 8' Description='Column 8' Category='File' Type='3' Hide_if='Variable(To file) == Const(0)' >
///			<Ranges>
///				<Range Value='plate instance' />
///				<Range Value='barcode South' />
///				<Range Value='barcode West' />
///				<Range Value='barcode North' />
///				<Range Value='barcode East' />
///				<Range Value='Empty' />
///				<Range Value='Do not use' />
///			</Ranges>
///		</Parameter>
///	  </Parameters>
///	</Command>
///</Velocity11>

function WriteToArray(arrayName, arrayIndex, content) {
    var x;
    var array_existing = false;
    for (x in jsAddin_glo2DArray) {
        if (x == arrayName) {
            array_existing = true;
            break;
        }
    }
    if (!array_existing) {
        jsAddin_glo2DArray[arrayName] = new Array();
    }

    var index = 0;
    if (arrayIndex == "plate instance") {
        index = plate.instance;
    }
    else {
        index = arrayIndex.valueOf();
    }

    var info
    switch (content) {
        case "barcode South":
            info = plate.barcode[0];
            break;
        case "barcode West":
            info = plate.barcode[1];
            break;
        case "barcode North":
            info = plate.barcode[2];
            break;
        case "barcode East":
            info = plate.barcode[3];
            break;
        default:
            info = content;
            break;
    }

    if (index >= 0) {
        jsAddin_glo2DArray[arrayName][index] = info;
    }

    return jsAddin_glo2DArray[arrayName];
}

function GetColumn(column) {
    if ("plate instance" == (column)) {
        return plate.instance;
    }
    else if ("barcode South" == (column)) {
        return plate.barcode[0];
    }
    else if ("barcode West" == (column)) {
        return plate.barcode[1];
    }
    else if ("barcode North" == (column)) {
        return plate.barcode[2];
    }
    else if ("barcode East" == (column)) {
        return plate.barcode[3];
    }
    else if ("Empty" == (column)) {
        return "";
    }
    else if ("Do not use" == (column)) {
        return "Do not use";
    }
    else {
        return column;
    }
}

function GetFileContent(separator, column1, column2, column3, column4, column5, column6, column7, column8) {
    var content = "";
    var part1, part2, part3, part4, part5, part6, part7, part8;
    if ("TAB" == separator) {
        separator = "\t";
    }
    part1 = GetColumn(column1);
    if ("Do not use" == column1) {
        return content;
    } else {
        content = part1
    }
    part2 = GetColumn(column2);
    if ("Do not use" == part2) {
        return content;
    } else {
        content = content + separator + part2;
    }
    part3 = GetColumn(column3);
    if ("Do not use" == part3) {
        return content;
    } else {
        content = content + separator + part3;
    }
    part4 = GetColumn(column4);
    if ("Do not use" == part4) {
        return content;
    } else {
        content = content + separator + part4;
    }
    part5 = GetColumn(column5);
    if ("Do not use" == part5) {
        return content;
    } else {
        content = content + separator + part5;
    }
    part6 = GetColumn(column6);
    if ("Do not use" == part6) {
        return content;
    } else {
        content = content + separator + part6;
    }
    part7 = GetColumn(column7);
    if ("Do not use" == part7) {
        return content;
    } else {
        content = content + separator + part7;
    }
    part8 = GetColumn(column8);
    if ("Do not use" == part8) {
        return content;
    } else {
        content = content + separator + part8;
    }
    return content;
}


function ReadAndStoreSymbol(toArray, arrayName, arrayIndex, content, toFile, fileName, append, saparator, col1, col2, col3, col4, col5, col6, col7, col8) {
    var the_array;
    if (toArray == "1")
        the_array= WriteToArray(arrayName, arrayIndex, content)

    //if 'To file' checkbox is unchecked, do nothing
    if ("0" == toFile) {
        return the_array;
    }

    var FileContent;
    //if 'append' checkbox is unchecked, overwrite the file at the first run.
    if ("1" != append && plate.instance == 1) {
        // Declare file object
        var file_obj = new File();
        // Opens, Writes, and Closes file.
        file_obj.Open(fileName, 'w');
        file_obj.Write("");
        file_obj.Close();
    }
    FileContent = GetFileContent(saparator, col1, col2, col3, col4, col5, col6, col7, col8);
    //judge the file have the newline
    var ReadFile;
    ReadFile = file_read(fileName);
    if ("" != ReadFile) {
        var HaveNewLine;
        HaveNewLine = ReadFile.indexOf("\n",ReadFile.length-1);
        if (-1 == HaveNewLine){
            file_write(fileName, "");
        }
    }
    file_write(fileName, FileContent);

    return the_array;
}
