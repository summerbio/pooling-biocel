///<?xml version='1.0' encoding='ASCII' ?>
///<Velocity11 file='MetaData' md5sum='30e0aedc9f717663755171f8f9456527' version='1.0' >
///	<Command Compiler='0' Description='JS importCsvToInventory' Editor='-1' Name='JS importCsvToInventory' NextTaskToExecute='1' RequiresRefresh='0' TaskRequiresLocation='0' VisibleAvailability='1' >
///	  <Parameters >
///		<Parameter Name='File name' Description='File name' Category='importCsvToInventory' ValueToDisplay='data file type(*.csv)|*.csv|' Scriptable='0' Type='9' />
///		<Parameter Name='Overwrite inventory' Description='Overwrite inventory' Category='importCsvToInventory' Type='0' Scriptable='0' />
///	  </Parameters>
///	</Command>
///</Velocity11>

function importCsvToInventory(fileName, overWrite)
{
	// Handle function importCsvToInventory.
	plateDB.importCsvToInventory(fileName, overWrite);
}
