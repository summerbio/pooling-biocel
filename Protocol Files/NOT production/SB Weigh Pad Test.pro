<?xml version='1.0' encoding='ASCII' ?>
<Velocity11 file='Protocol_Data' md5sum='4b3ebaa1dffc2f48c68ad95c91df516e' version='2.0' >
	<File_Info AllowSimultaneousRun='0' AutoExportGanttChart='0' AutoLoadRacks='When the main protocol starts' AutoUnloadRacks='1' AutomaticallyLoadFormFile='1' Barcodes_Directory='' ClearInventory='0' DeleteHitpickFiles='1' Description='' Device_File='C:\vworks workspace\Device Files\Pooling.dev' Display_User_Task_Descriptions='1' DynamicAssignPlateStorageLoad='0' FinishScript='' Form_File='C:\vworks workspace\Form Files\Pooling.VWForm' HandlePlatesInInstance='1' ImportInventory='0' InventoryFile='' Notes='' PipettePlatesInInstanceOrder='0' Protocol_Alias='' StartScript='' Use_Global_JS_Context='1' />
	<Processes >
		<Startup_Processes >
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::JavaScript' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='open(&quot;c:\\vworks workspace\\Rigpa\\Init.js&quot;);' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Load Scripts' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::JavaScript' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='if (!InitSuccess)
{
    task.pause();
    print(&quot;Silent script failure when loading javascript. ABORT PROTOCOL!&quot;); 
}' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Check For Silent Script Failure During Script Loading' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='startup process - 1' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
		</Startup_Processes>
		<Main_Processes >
			<Process >
				<Minimized >0</Minimized>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='Beckhoff IO Module - 1' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='VACUUM RNA BUFFER' />
						<Parameter Category='' Name='On / Off' Value='On' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:1:2' />
						<Parameter Name='Pin name saved' Value='VACUUM RNA BUFFER' />
						<Parameter Category='Task Description' Name='Task number' Value='1' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Task Name='SummerBioOhausWeighPadDriver::Read' >
					<Devices >
						<Device Device_Name='WeighPadRNABuffer' Location_Name='Default Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='E(&quot;PoolingMicroFill.WeighPadPreDispense();&quot;);
' />
					<Parameters >
						<Parameter Category='' Name='Check Threshold' Value='0' />
						<Parameter Category='' Name='Threshold Min' Value='' />
						<Parameter Category='' Name='Threshold Max' Value='' />
						<Parameter Category='' Name='Check Difference' Value='' />
						<Parameter Category='' Name='Difference Max' Value='' />
						<Parameter Category='' Name='Difference Min' Value='' />
						<Parameter Category='' Name='Variable Name' Value='' />
						<Parameter Category='' Name='Difference Variable Name' Value='' />
						<Parameter Category='' Name='Difference Key' Value='' />
						<Parameter Category='Task Description' Name='Task number' Value='2' />
						<Parameter Category='Task Description' Name='Task description' Value='Pre-Dispense Read' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::JavaScript' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='E(&quot;PoolingMicroFill.WeighPadPreDispenseDone();&quot;);' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='3' />
						<Parameter Category='Task Description' Name='Task description' Value='Done Pre-Dispense' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BioTek MicroFill::Dispense' >
					<Devices >
						<Device Device_Name='1MicroFill-1' Location_Name='Stage' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='24' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='E(&quot;PoolingMicroFill.DispenseRNABuffer();&quot;);' />
					<Parameters >
						<Parameter Category='' Name='Run On-Board Program' Value='0' />
						<Parameter Category='' Name='User-Defined Program' Value='38mL prime' />
						<Parameter Category='' Name='On-board Program' Value='600' />
						<Parameter Category='' Name='Program timeout' Value='600' />
						<Parameter Category='Task Description' Name='Task number' Value='4' />
						<Parameter Category='Task Description' Name='Task description' Value='Dispense RNA Buffer' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='SummerBioOhausWeighPadDriver::Read' >
					<Devices >
						<Device Device_Name='WeighPadRNABuffer' Location_Name='Default Location' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='4' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='E(&quot;PoolingMicroFill.WeighPadPostDispense();&quot;);' />
					<Parameters >
						<Parameter Category='' Name='Check Threshold' Value='' />
						<Parameter Category='' Name='Threshold Min' Value='' />
						<Parameter Category='' Name='Threshold Max' Value='' />
						<Parameter Category='' Name='Check Difference' Value='0' />
						<Parameter Category='' Name='Difference Max' Value='' />
						<Parameter Category='' Name='Difference Min' Value='' />
						<Parameter Category='' Name='Variable Name' Value='' />
						<Parameter Category='' Name='Difference Variable Name' Value='' />
						<Parameter Category='' Name='Difference Key' Value='' />
						<Parameter Category='Task Description' Name='Task number' Value='5' />
						<Parameter Category='Task Description' Name='Task description' Value='Post-Dispense Read' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::JavaScript' >
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='E(&quot;PoolingMicroFill.WeighPadPostDispenseDone();&quot;);
' />
					<Parameters >
						<Parameter Category='Task Description' Name='Task number' Value='6' />
						<Parameter Category='Task Description' Name='Task description' Value='Done Post-Dispense Read' />
						<Parameter Category='Task Description' Name='Use default task description' Value='0' />
					</Parameters>
				</Task>
				<Task Name='BuiltIn::Digital Output' >
					<Devices >
						<Device Device_Name='Beckhoff IO Module - 1' Location_Name='' />
					</Devices>
					<Enable_Backup >0</Enable_Backup>
					<Task_Disabled >0</Task_Disabled>
					<Task_Skipped >0</Task_Skipped>
					<Has_Breakpoint >0</Has_Breakpoint>
					<Advanced_Settings >
						<Setting Name='Estimated time' Value='0' />
					</Advanced_Settings>
					<TaskScript Name='TaskScript' Value='' />
					<Parameters >
						<Parameter Category='' Name='Digital output name' Value='VACUUM RNA BUFFER' />
						<Parameter Category='' Name='On / Off' Value='Off' />
						<Parameter Category='' Name='Wait for time' Value='0' />
						<Parameter Category='' Name='Duration of On / Off' Value='0' />
						<Parameter Name='Device and Bit Name' Value='DC24V_PS:1:2' />
						<Parameter Name='Pin name saved' Value='VACUUM RNA BUFFER' />
						<Parameter Category='Task Description' Name='Task number' Value='7' />
						<Parameter Category='Task Description' Name='Task description' Value='Digital Output' />
						<Parameter Category='Task Description' Name='Use default task description' Value='1' />
					</Parameters>
				</Task>
				<Plate_Parameters >
					<Parameter Name='Plate name' Value='process - 1' />
					<Parameter Name='Plate type' Value='96 1.4ml Micronic Tube Rack External Thread WITH Caps' />
					<Parameter Name='Simultaneous plates' Value='1' />
					<Parameter Name='Plates have lids' Value='0' />
					<Parameter Name='Plates enter the system sealed' Value='0' />
					<Parameter Name='Use single instance of plate' Value='0' />
					<Parameter Name='Automatically update labware' Value='0' />
					<Parameter Name='Enable timed release' Value='0' />
					<Parameter Name='Release time' Value='30' />
					<Parameter Name='Auto managed counterweight' Value='0' />
					<Parameter Name='Barcode filename' Value='No Selection' />
					<Parameter Name='Has header' Value='' />
					<Parameter Name='Barcode or header South' Value='No Selection' />
					<Parameter Name='Barcode or header West' Value='No Selection' />
					<Parameter Name='Barcode or header North' Value='No Selection' />
					<Parameter Name='Barcode or header East' Value='No Selection' />
				</Plate_Parameters>
				<Quarantine_After_Process >0</Quarantine_After_Process>
			</Process>
		</Main_Processes>
	</Processes>
</Velocity11>