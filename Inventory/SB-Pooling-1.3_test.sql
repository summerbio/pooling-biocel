-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: velocity11
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `custom_data`
--

DROP TABLE IF EXISTS `custom_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_data` (
  `plate_id` int NOT NULL,
  `custom_key` varchar(255) NOT NULL DEFAULT '',
  `custom_value` varchar(255) NOT NULL DEFAULT '',
  KEY `plate_id` (`plate_id`),
  CONSTRAINT `custom_data_ibfk_1` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_data`
--

LOCK TABLES `custom_data` WRITE;
/*!40000 ALTER TABLE `custom_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devices` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_name` varchar(255) NOT NULL DEFAULT '',
  `device_type` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_name` (`device_name`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'1Conveyor-1',''),(2,'1Conveyor-2',''),(3,'1Conveyor-3',''),(4,'1Conveyor-4',''),(5,'1Conveyor-5',''),(6,'1Conveyor-6',''),(7,'1Decapper-1',''),(8,'1Decapper-1_Pad',''),(9,'1Decapper-2',''),(10,'1Decapper-2_Pad',''),(11,'1Decapper-3',''),(12,'1Decapper-3_Pad',''),(13,'1DirectDriveRobot-1',''),(14,'1Hotel-1',''),(15,'1MicroFill-1',''),(16,'1MiniHub-1','StoreXDevice'),(17,'1Pad-1',''),(18,'1Pad-2',''),(19,'1Pad-3',''),(20,'1Pad-4',''),(21,'1VPrep-1',''),(22,'1ZiathScanner-1',''),(23,'Agilent Phantom Stacker - 1',''),(24,'Agilent Phantom Stacker - 2',''),(25,'Agilent Phantom Stacker - 3',''),(26,'Agilent Phantom Stacker - 4',''),(27,'Agilent Phantom Stacker - 5',''),(28,'Agilent Phantom Stacker - 6',''),(29,'ZiathPad',''),(30,'ZiathPad2',''),(31,'ZiathPad3','');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_data`
--

DROP TABLE IF EXISTS `global_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `global_data` (
  `global_key` varchar(255) NOT NULL,
  `global_value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`global_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_data`
--

LOCK TABLES `global_data` WRITE;
/*!40000 ALTER TABLE `global_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `global_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` int NOT NULL,
  `device_specific_location_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (8,16,1),(9,16,2),(10,16,3),(11,16,4),(12,16,5),(13,16,6),(15,16,7),(16,16,8),(17,16,9),(18,16,10),(19,16,11),(20,16,12);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plategroup_names`
--

DROP TABLE IF EXISTS `plategroup_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plategroup_names` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` int NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plategroup_names`
--

LOCK TABLES `plategroup_names` WRITE;
/*!40000 ALTER TABLE `plategroup_names` DISABLE KEYS */;
INSERT INTO `plategroup_names` VALUES (1,1,'Discrete Racks'),(2,1,'Discrete Tip Boxes'),(3,1,'Pooled Racks');
/*!40000 ALTER TABLE `plategroup_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plategroups`
--

DROP TABLE IF EXISTS `plategroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plategroups` (
  `group_id` int NOT NULL DEFAULT '0',
  `plate_id` int DEFAULT '0',
  `plate_index` int DEFAULT '0',
  `cassette` int DEFAULT '0',
  `slot` int DEFAULT '0',
  `device_name` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `group_id_2` (`group_id`,`plate_index`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `plategroups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `plategroup_names` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plategroups`
--

LOCK TABLES `plategroups` WRITE;
/*!40000 ALTER TABLE `plategroups` DISABLE KEYS */;
INSERT INTO `plategroups` VALUES (1,9,0,-1,-1,'1MiniHub-1'),(1,10,1,-1,-1,'1MiniHub-1'),(1,11,2,-1,-1,'1MiniHub-1'),(1,12,3,-1,-1,'1MiniHub-1'),(1,13,4,-1,-1,'1MiniHub-1'),(2,14,0,-1,-1,'1MiniHub-1'),(2,16,1,-1,-1,'1MiniHub-1'),(3,17,0,-1,-1,'1MiniHub-1'),(3,18,1,-1,-1,'1MiniHub-1'),(2,19,2,-1,-1,'1MiniHub-1'),(2,20,3,-1,-1,'1MiniHub-1'),(2,21,4,-1,-1,'1MiniHub-1');
/*!40000 ALTER TABLE `plategroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plates`
--

DROP TABLE IF EXISTS `plates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plates` (
  `id` int NOT NULL AUTO_INCREMENT,
  `plate_name` varchar(255) NOT NULL DEFAULT '',
  `labware` varchar(255) NOT NULL DEFAULT '',
  `northbc` varchar(255) NOT NULL DEFAULT '',
  `eastbc` varchar(255) NOT NULL DEFAULT '',
  `southbc` varchar(255) NOT NULL DEFAULT '',
  `westbc` varchar(255) NOT NULL DEFAULT '',
  `parent` int NOT NULL DEFAULT '0',
  `destination_location_id` int NOT NULL DEFAULT '0',
  `current_location_id` int NOT NULL DEFAULT '0',
  `protocol_id` int NOT NULL DEFAULT '0',
  `sealed` int NOT NULL DEFAULT '0',
  `lidded` int NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  `created_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plates`
--

LOCK TABLES `plates` WRITE;
/*!40000 ALTER TABLE `plates` DISABLE KEYS */;
INSERT INTO `plates` VALUES (9,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,8,0,0,0,0,'2021-09-15 22:24:49.969'),(10,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,9,0,0,0,0,'2021-09-15 22:25:01.848'),(11,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,10,0,0,0,0,'2021-09-15 22:25:13.280'),(12,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,11,0,0,0,0,'2021-09-15 22:25:23.288'),(13,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,12,0,0,0,0,'2021-09-15 22:25:37.912'),(14,'','96 180ul Filtered Delidded','','','','',0,0,13,0,0,0,0,'2021-09-15 22:25:54.584'),(16,'','96 180ul Filtered Delidded','','','','',0,0,15,0,0,0,0,'2021-09-15 22:26:27.729'),(17,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,16,0,0,0,0,'2021-09-15 22:26:41.303'),(18,'','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,17,0,0,0,0,'2021-09-15 22:26:59.736'),(19,'','96 180ul Filtered Delidded','','','','',0,0,18,0,0,0,0,'2021-09-21 21:36:11.431'),(20,'','96 180ul Filtered Delidded','','','','',0,0,19,0,0,0,0,'2021-09-21 21:36:33.198'),(21,'','96 180ul Filtered Delidded','','','','',0,0,20,0,0,0,0,'2021-09-21 21:36:50.725');
/*!40000 ALTER TABLE `plates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protocol_steps`
--

DROP TABLE IF EXISTS `protocol_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `protocol_steps` (
  `plate_id` int NOT NULL,
  `state` int NOT NULL DEFAULT '0',
  `step` int NOT NULL DEFAULT '0',
  `substep` int NOT NULL DEFAULT '0',
  `start_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  UNIQUE KEY `plate_id` (`plate_id`),
  CONSTRAINT `protocol_steps_ibfk_1` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protocol_steps`
--

LOCK TABLES `protocol_steps` WRITE;
/*!40000 ALTER TABLE `protocol_steps` DISABLE KEYS */;
INSERT INTO `protocol_steps` VALUES (9,0,0,0,'2021-09-15 22:24:49.972'),(10,0,0,0,'2021-09-15 22:25:01.851'),(11,0,0,0,'2021-09-15 22:25:13.282'),(12,0,0,0,'2021-09-15 22:25:23.290'),(13,0,0,0,'2021-09-15 22:25:37.914'),(14,0,0,0,'2021-09-15 22:25:54.586'),(16,0,0,0,'2021-09-15 22:26:27.731'),(17,0,0,0,'2021-09-15 22:26:41.306'),(18,0,0,0,'2021-09-15 22:26:59.738'),(19,0,0,0,'2021-09-21 21:36:11.434'),(20,0,0,0,'2021-09-21 21:36:33.200'),(21,0,0,0,'2021-09-21 21:36:50.728');
/*!40000 ALTER TABLE `protocol_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protocols`
--

DROP TABLE IF EXISTS `protocols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `protocols` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_file` varchar(255) NOT NULL DEFAULT '',
  `protocol_file` varchar(255) NOT NULL DEFAULT '',
  `start_time` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_file` (`device_file`),
  UNIQUE KEY `protocol_file` (`protocol_file`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protocols`
--

LOCK TABLES `protocols` WRITE;
/*!40000 ALTER TABLE `protocols` DISABLE KEYS */;
/*!40000 ALTER TABLE `protocols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_info`
--

DROP TABLE IF EXISTS `schema_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schema_info` (
  `schema_key` varchar(255) NOT NULL,
  `schema_value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`schema_key`),
  UNIQUE KEY `schema_key` (`schema_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_info`
--

LOCK TABLES `schema_info` WRITE;
/*!40000 ALTER TABLE `schema_info` DISABLE KEYS */;
INSERT INTO `schema_info` VALUES ('version','2');
/*!40000 ALTER TABLE `schema_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storexdevice`
--

DROP TABLE IF EXISTS `storexdevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storexdevice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` int NOT NULL,
  `cassette` int NOT NULL DEFAULT '0',
  `slot` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `storexdevice_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storexdevice`
--

LOCK TABLES `storexdevice` WRITE;
/*!40000 ALTER TABLE `storexdevice` DISABLE KEYS */;
INSERT INTO `storexdevice` VALUES (1,16,1,1),(2,16,1,2),(3,16,1,3),(4,16,1,4),(5,16,1,5),(6,16,2,1),(7,16,2,2),(8,16,3,1),(9,16,3,2),(10,16,2,3),(11,16,2,4),(12,16,2,5);
/*!40000 ALTER TABLE `storexdevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tube_records`
--

DROP TABLE IF EXISTS `tube_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tube_records` (
  `plate_id` int NOT NULL,
  `wellrow` int NOT NULL,
  `wellcol` int NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `weight_g` double NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  UNIQUE KEY `barcode` (`barcode`),
  KEY `plate_id` (`plate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tube_records`
--

LOCK TABLES `tube_records` WRITE;
/*!40000 ALTER TABLE `tube_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `tube_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `volumes` (
  `plate_id` int NOT NULL,
  `wellrow` int NOT NULL,
  `wellcol` int NOT NULL,
  `volume` double NOT NULL,
  KEY `plate_id` (`plate_id`),
  CONSTRAINT `volumes_ibfk_1` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
INSERT INTO `volumes` VALUES (9,0,0,0),(9,0,1,0),(9,0,2,0),(9,0,3,0),(9,0,4,0),(9,0,5,0),(9,0,6,0),(9,0,7,0),(9,0,8,0),(9,0,9,0),(9,0,10,0),(9,0,11,0),(9,1,0,0),(9,1,1,0),(9,1,2,0),(9,1,3,0),(9,1,4,0),(9,1,5,0),(9,1,6,0),(9,1,7,0),(9,1,8,0),(9,1,9,0),(9,1,10,0),(9,1,11,0),(9,2,0,0),(9,2,1,0),(9,2,2,0),(9,2,3,0),(9,2,4,0),(9,2,5,0),(9,2,6,0),(9,2,7,0),(9,2,8,0),(9,2,9,0),(9,2,10,0),(9,2,11,0),(9,3,0,0),(9,3,1,0),(9,3,2,0),(9,3,3,0),(9,3,4,0),(9,3,5,0),(9,3,6,0),(9,3,7,0),(9,3,8,0),(9,3,9,0),(9,3,10,0),(9,3,11,0),(9,4,0,0),(9,4,1,0),(9,4,2,0),(9,4,3,0),(9,4,4,0),(9,4,5,0),(9,4,6,0),(9,4,7,0),(9,4,8,0),(9,4,9,0),(9,4,10,0),(9,4,11,0),(9,5,0,0),(9,5,1,0),(9,5,2,0),(9,5,3,0),(9,5,4,0),(9,5,5,0),(9,5,6,0),(9,5,7,0),(9,5,8,0),(9,5,9,0),(9,5,10,0),(9,5,11,0),(9,6,0,0),(9,6,1,0),(9,6,2,0),(9,6,3,0),(9,6,4,0),(9,6,5,0),(9,6,6,0),(9,6,7,0),(9,6,8,0),(9,6,9,0),(9,6,10,0),(9,6,11,0),(9,7,0,0),(9,7,1,0),(9,7,2,0),(9,7,3,0),(9,7,4,0),(9,7,5,0),(9,7,6,0),(9,7,7,0),(9,7,8,0),(9,7,9,0),(9,7,10,0),(9,7,11,0),(10,0,0,0),(10,0,1,0),(10,0,2,0),(10,0,3,0),(10,0,4,0),(10,0,5,0),(10,0,6,0),(10,0,7,0),(10,0,8,0),(10,0,9,0),(10,0,10,0),(10,0,11,0),(10,1,0,0),(10,1,1,0),(10,1,2,0),(10,1,3,0),(10,1,4,0),(10,1,5,0),(10,1,6,0),(10,1,7,0),(10,1,8,0),(10,1,9,0),(10,1,10,0),(10,1,11,0),(10,2,0,0),(10,2,1,0),(10,2,2,0),(10,2,3,0),(10,2,4,0),(10,2,5,0),(10,2,6,0),(10,2,7,0),(10,2,8,0),(10,2,9,0),(10,2,10,0),(10,2,11,0),(10,3,0,0),(10,3,1,0),(10,3,2,0),(10,3,3,0),(10,3,4,0),(10,3,5,0),(10,3,6,0),(10,3,7,0),(10,3,8,0),(10,3,9,0),(10,3,10,0),(10,3,11,0),(10,4,0,0),(10,4,1,0),(10,4,2,0),(10,4,3,0),(10,4,4,0),(10,4,5,0),(10,4,6,0),(10,4,7,0),(10,4,8,0),(10,4,9,0),(10,4,10,0),(10,4,11,0),(10,5,0,0),(10,5,1,0),(10,5,2,0),(10,5,3,0),(10,5,4,0),(10,5,5,0),(10,5,6,0),(10,5,7,0),(10,5,8,0),(10,5,9,0),(10,5,10,0),(10,5,11,0),(10,6,0,0),(10,6,1,0),(10,6,2,0),(10,6,3,0),(10,6,4,0),(10,6,5,0),(10,6,6,0),(10,6,7,0),(10,6,8,0),(10,6,9,0),(10,6,10,0),(10,6,11,0),(10,7,0,0),(10,7,1,0),(10,7,2,0),(10,7,3,0),(10,7,4,0),(10,7,5,0),(10,7,6,0),(10,7,7,0),(10,7,8,0),(10,7,9,0),(10,7,10,0),(10,7,11,0),(11,0,0,0),(11,0,1,0),(11,0,2,0),(11,0,3,0),(11,0,4,0),(11,0,5,0),(11,0,6,0),(11,0,7,0),(11,0,8,0),(11,0,9,0),(11,0,10,0),(11,0,11,0),(11,1,0,0),(11,1,1,0),(11,1,2,0),(11,1,3,0),(11,1,4,0),(11,1,5,0),(11,1,6,0),(11,1,7,0),(11,1,8,0),(11,1,9,0),(11,1,10,0),(11,1,11,0),(11,2,0,0),(11,2,1,0),(11,2,2,0),(11,2,3,0),(11,2,4,0),(11,2,5,0),(11,2,6,0),(11,2,7,0),(11,2,8,0),(11,2,9,0),(11,2,10,0),(11,2,11,0),(11,3,0,0),(11,3,1,0),(11,3,2,0),(11,3,3,0),(11,3,4,0),(11,3,5,0),(11,3,6,0),(11,3,7,0),(11,3,8,0),(11,3,9,0),(11,3,10,0),(11,3,11,0),(11,4,0,0),(11,4,1,0),(11,4,2,0),(11,4,3,0),(11,4,4,0),(11,4,5,0),(11,4,6,0),(11,4,7,0),(11,4,8,0),(11,4,9,0),(11,4,10,0),(11,4,11,0),(11,5,0,0),(11,5,1,0),(11,5,2,0),(11,5,3,0),(11,5,4,0),(11,5,5,0),(11,5,6,0),(11,5,7,0),(11,5,8,0),(11,5,9,0),(11,5,10,0),(11,5,11,0),(11,6,0,0),(11,6,1,0),(11,6,2,0),(11,6,3,0),(11,6,4,0),(11,6,5,0),(11,6,6,0),(11,6,7,0),(11,6,8,0),(11,6,9,0),(11,6,10,0),(11,6,11,0),(11,7,0,0),(11,7,1,0),(11,7,2,0),(11,7,3,0),(11,7,4,0),(11,7,5,0),(11,7,6,0),(11,7,7,0),(11,7,8,0),(11,7,9,0),(11,7,10,0),(11,7,11,0),(12,0,0,0),(12,0,1,0),(12,0,2,0),(12,0,3,0),(12,0,4,0),(12,0,5,0),(12,0,6,0),(12,0,7,0),(12,0,8,0),(12,0,9,0),(12,0,10,0),(12,0,11,0),(12,1,0,0),(12,1,1,0),(12,1,2,0),(12,1,3,0),(12,1,4,0),(12,1,5,0),(12,1,6,0),(12,1,7,0),(12,1,8,0),(12,1,9,0),(12,1,10,0),(12,1,11,0),(12,2,0,0),(12,2,1,0),(12,2,2,0),(12,2,3,0),(12,2,4,0),(12,2,5,0),(12,2,6,0),(12,2,7,0),(12,2,8,0),(12,2,9,0),(12,2,10,0),(12,2,11,0),(12,3,0,0),(12,3,1,0),(12,3,2,0),(12,3,3,0),(12,3,4,0),(12,3,5,0),(12,3,6,0),(12,3,7,0),(12,3,8,0),(12,3,9,0),(12,3,10,0),(12,3,11,0),(12,4,0,0),(12,4,1,0),(12,4,2,0),(12,4,3,0),(12,4,4,0),(12,4,5,0),(12,4,6,0),(12,4,7,0),(12,4,8,0),(12,4,9,0),(12,4,10,0),(12,4,11,0),(12,5,0,0),(12,5,1,0),(12,5,2,0),(12,5,3,0),(12,5,4,0),(12,5,5,0),(12,5,6,0),(12,5,7,0),(12,5,8,0),(12,5,9,0),(12,5,10,0),(12,5,11,0),(12,6,0,0),(12,6,1,0),(12,6,2,0),(12,6,3,0),(12,6,4,0),(12,6,5,0),(12,6,6,0),(12,6,7,0),(12,6,8,0),(12,6,9,0),(12,6,10,0),(12,6,11,0),(12,7,0,0),(12,7,1,0),(12,7,2,0),(12,7,3,0),(12,7,4,0),(12,7,5,0),(12,7,6,0),(12,7,7,0),(12,7,8,0),(12,7,9,0),(12,7,10,0),(12,7,11,0),(13,0,0,0),(13,0,1,0),(13,0,2,0),(13,0,3,0),(13,0,4,0),(13,0,5,0),(13,0,6,0),(13,0,7,0),(13,0,8,0),(13,0,9,0),(13,0,10,0),(13,0,11,0),(13,1,0,0),(13,1,1,0),(13,1,2,0),(13,1,3,0),(13,1,4,0),(13,1,5,0),(13,1,6,0),(13,1,7,0),(13,1,8,0),(13,1,9,0),(13,1,10,0),(13,1,11,0),(13,2,0,0),(13,2,1,0),(13,2,2,0),(13,2,3,0),(13,2,4,0),(13,2,5,0),(13,2,6,0),(13,2,7,0),(13,2,8,0),(13,2,9,0),(13,2,10,0),(13,2,11,0),(13,3,0,0),(13,3,1,0),(13,3,2,0),(13,3,3,0),(13,3,4,0),(13,3,5,0),(13,3,6,0),(13,3,7,0),(13,3,8,0),(13,3,9,0),(13,3,10,0),(13,3,11,0),(13,4,0,0),(13,4,1,0),(13,4,2,0),(13,4,3,0),(13,4,4,0),(13,4,5,0),(13,4,6,0),(13,4,7,0),(13,4,8,0),(13,4,9,0),(13,4,10,0),(13,4,11,0),(13,5,0,0),(13,5,1,0),(13,5,2,0),(13,5,3,0),(13,5,4,0),(13,5,5,0),(13,5,6,0),(13,5,7,0),(13,5,8,0),(13,5,9,0),(13,5,10,0),(13,5,11,0),(13,6,0,0),(13,6,1,0),(13,6,2,0),(13,6,3,0),(13,6,4,0),(13,6,5,0),(13,6,6,0),(13,6,7,0),(13,6,8,0),(13,6,9,0),(13,6,10,0),(13,6,11,0),(13,7,0,0),(13,7,1,0),(13,7,2,0),(13,7,3,0),(13,7,4,0),(13,7,5,0),(13,7,6,0),(13,7,7,0),(13,7,8,0),(13,7,9,0),(13,7,10,0),(13,7,11,0),(14,0,0,0),(14,0,1,0),(14,0,2,0),(14,0,3,0),(14,0,4,0),(14,0,5,0),(14,0,6,0),(14,0,7,0),(14,0,8,0),(14,0,9,0),(14,0,10,0),(14,0,11,0),(14,1,0,0),(14,1,1,0),(14,1,2,0),(14,1,3,0),(14,1,4,0),(14,1,5,0),(14,1,6,0),(14,1,7,0),(14,1,8,0),(14,1,9,0),(14,1,10,0),(14,1,11,0),(14,2,0,0),(14,2,1,0),(14,2,2,0),(14,2,3,0),(14,2,4,0),(14,2,5,0),(14,2,6,0),(14,2,7,0),(14,2,8,0),(14,2,9,0),(14,2,10,0),(14,2,11,0),(14,3,0,0),(14,3,1,0),(14,3,2,0),(14,3,3,0),(14,3,4,0),(14,3,5,0),(14,3,6,0),(14,3,7,0),(14,3,8,0),(14,3,9,0),(14,3,10,0),(14,3,11,0),(14,4,0,0),(14,4,1,0),(14,4,2,0),(14,4,3,0),(14,4,4,0),(14,4,5,0),(14,4,6,0),(14,4,7,0),(14,4,8,0),(14,4,9,0),(14,4,10,0),(14,4,11,0),(14,5,0,0),(14,5,1,0),(14,5,2,0),(14,5,3,0),(14,5,4,0),(14,5,5,0),(14,5,6,0),(14,5,7,0),(14,5,8,0),(14,5,9,0),(14,5,10,0),(14,5,11,0),(14,6,0,0),(14,6,1,0),(14,6,2,0),(14,6,3,0),(14,6,4,0),(14,6,5,0),(14,6,6,0),(14,6,7,0),(14,6,8,0),(14,6,9,0),(14,6,10,0),(14,6,11,0),(14,7,0,0),(14,7,1,0),(14,7,2,0),(14,7,3,0),(14,7,4,0),(14,7,5,0),(14,7,6,0),(14,7,7,0),(14,7,8,0),(14,7,9,0),(14,7,10,0),(14,7,11,0),(16,0,0,0),(16,0,1,0),(16,0,2,0),(16,0,3,0),(16,0,4,0),(16,0,5,0),(16,0,6,0),(16,0,7,0),(16,0,8,0),(16,0,9,0),(16,0,10,0),(16,0,11,0),(16,1,0,0),(16,1,1,0),(16,1,2,0),(16,1,3,0),(16,1,4,0),(16,1,5,0),(16,1,6,0),(16,1,7,0),(16,1,8,0),(16,1,9,0),(16,1,10,0),(16,1,11,0),(16,2,0,0),(16,2,1,0),(16,2,2,0),(16,2,3,0),(16,2,4,0),(16,2,5,0),(16,2,6,0),(16,2,7,0),(16,2,8,0),(16,2,9,0),(16,2,10,0),(16,2,11,0),(16,3,0,0),(16,3,1,0),(16,3,2,0),(16,3,3,0),(16,3,4,0),(16,3,5,0),(16,3,6,0),(16,3,7,0),(16,3,8,0),(16,3,9,0),(16,3,10,0),(16,3,11,0),(16,4,0,0),(16,4,1,0),(16,4,2,0),(16,4,3,0),(16,4,4,0),(16,4,5,0),(16,4,6,0),(16,4,7,0),(16,4,8,0),(16,4,9,0),(16,4,10,0),(16,4,11,0),(16,5,0,0),(16,5,1,0),(16,5,2,0),(16,5,3,0),(16,5,4,0),(16,5,5,0),(16,5,6,0),(16,5,7,0),(16,5,8,0),(16,5,9,0),(16,5,10,0),(16,5,11,0),(16,6,0,0),(16,6,1,0),(16,6,2,0),(16,6,3,0),(16,6,4,0),(16,6,5,0),(16,6,6,0),(16,6,7,0),(16,6,8,0),(16,6,9,0),(16,6,10,0),(16,6,11,0),(16,7,0,0),(16,7,1,0),(16,7,2,0),(16,7,3,0),(16,7,4,0),(16,7,5,0),(16,7,6,0),(16,7,7,0),(16,7,8,0),(16,7,9,0),(16,7,10,0),(16,7,11,0),(17,0,0,0),(17,0,1,0),(17,0,2,0),(17,0,3,0),(17,0,4,0),(17,0,5,0),(17,0,6,0),(17,0,7,0),(17,0,8,0),(17,0,9,0),(17,0,10,0),(17,0,11,0),(17,1,0,0),(17,1,1,0),(17,1,2,0),(17,1,3,0),(17,1,4,0),(17,1,5,0),(17,1,6,0),(17,1,7,0),(17,1,8,0),(17,1,9,0),(17,1,10,0),(17,1,11,0),(17,2,0,0),(17,2,1,0),(17,2,2,0),(17,2,3,0),(17,2,4,0),(17,2,5,0),(17,2,6,0),(17,2,7,0),(17,2,8,0),(17,2,9,0),(17,2,10,0),(17,2,11,0),(17,3,0,0),(17,3,1,0),(17,3,2,0),(17,3,3,0),(17,3,4,0),(17,3,5,0),(17,3,6,0),(17,3,7,0),(17,3,8,0),(17,3,9,0),(17,3,10,0),(17,3,11,0),(17,4,0,0),(17,4,1,0),(17,4,2,0),(17,4,3,0),(17,4,4,0),(17,4,5,0),(17,4,6,0),(17,4,7,0),(17,4,8,0),(17,4,9,0),(17,4,10,0),(17,4,11,0),(17,5,0,0),(17,5,1,0),(17,5,2,0),(17,5,3,0),(17,5,4,0),(17,5,5,0),(17,5,6,0),(17,5,7,0),(17,5,8,0),(17,5,9,0),(17,5,10,0),(17,5,11,0),(17,6,0,0),(17,6,1,0),(17,6,2,0),(17,6,3,0),(17,6,4,0),(17,6,5,0),(17,6,6,0),(17,6,7,0),(17,6,8,0),(17,6,9,0),(17,6,10,0),(17,6,11,0),(17,7,0,0),(17,7,1,0),(17,7,2,0),(17,7,3,0),(17,7,4,0),(17,7,5,0),(17,7,6,0),(17,7,7,0),(17,7,8,0),(17,7,9,0),(17,7,10,0),(17,7,11,0),(18,0,0,0),(18,0,1,0),(18,0,2,0),(18,0,3,0),(18,0,4,0),(18,0,5,0),(18,0,6,0),(18,0,7,0),(18,0,8,0),(18,0,9,0),(18,0,10,0),(18,0,11,0),(18,1,0,0),(18,1,1,0),(18,1,2,0),(18,1,3,0),(18,1,4,0),(18,1,5,0),(18,1,6,0),(18,1,7,0),(18,1,8,0),(18,1,9,0),(18,1,10,0),(18,1,11,0),(18,2,0,0),(18,2,1,0),(18,2,2,0),(18,2,3,0),(18,2,4,0),(18,2,5,0),(18,2,6,0),(18,2,7,0),(18,2,8,0),(18,2,9,0),(18,2,10,0),(18,2,11,0),(18,3,0,0),(18,3,1,0),(18,3,2,0),(18,3,3,0),(18,3,4,0),(18,3,5,0),(18,3,6,0),(18,3,7,0),(18,3,8,0),(18,3,9,0),(18,3,10,0),(18,3,11,0),(18,4,0,0),(18,4,1,0),(18,4,2,0),(18,4,3,0),(18,4,4,0),(18,4,5,0),(18,4,6,0),(18,4,7,0),(18,4,8,0),(18,4,9,0),(18,4,10,0),(18,4,11,0),(18,5,0,0),(18,5,1,0),(18,5,2,0),(18,5,3,0),(18,5,4,0),(18,5,5,0),(18,5,6,0),(18,5,7,0),(18,5,8,0),(18,5,9,0),(18,5,10,0),(18,5,11,0),(18,6,0,0),(18,6,1,0),(18,6,2,0),(18,6,3,0),(18,6,4,0),(18,6,5,0),(18,6,6,0),(18,6,7,0),(18,6,8,0),(18,6,9,0),(18,6,10,0),(18,6,11,0),(18,7,0,0),(18,7,1,0),(18,7,2,0),(18,7,3,0),(18,7,4,0),(18,7,5,0),(18,7,6,0),(18,7,7,0),(18,7,8,0),(18,7,9,0),(18,7,10,0),(18,7,11,0),(19,0,0,0),(19,0,1,0),(19,0,2,0),(19,0,3,0),(19,0,4,0),(19,0,5,0),(19,0,6,0),(19,0,7,0),(19,0,8,0),(19,0,9,0),(19,0,10,0),(19,0,11,0),(19,1,0,0),(19,1,1,0),(19,1,2,0),(19,1,3,0),(19,1,4,0),(19,1,5,0),(19,1,6,0),(19,1,7,0),(19,1,8,0),(19,1,9,0),(19,1,10,0),(19,1,11,0),(19,2,0,0),(19,2,1,0),(19,2,2,0),(19,2,3,0),(19,2,4,0),(19,2,5,0),(19,2,6,0),(19,2,7,0),(19,2,8,0),(19,2,9,0),(19,2,10,0),(19,2,11,0),(19,3,0,0),(19,3,1,0),(19,3,2,0),(19,3,3,0),(19,3,4,0),(19,3,5,0),(19,3,6,0),(19,3,7,0),(19,3,8,0),(19,3,9,0),(19,3,10,0),(19,3,11,0),(19,4,0,0),(19,4,1,0),(19,4,2,0),(19,4,3,0),(19,4,4,0),(19,4,5,0),(19,4,6,0),(19,4,7,0),(19,4,8,0),(19,4,9,0),(19,4,10,0),(19,4,11,0),(19,5,0,0),(19,5,1,0),(19,5,2,0),(19,5,3,0),(19,5,4,0),(19,5,5,0),(19,5,6,0),(19,5,7,0),(19,5,8,0),(19,5,9,0),(19,5,10,0),(19,5,11,0),(19,6,0,0),(19,6,1,0),(19,6,2,0),(19,6,3,0),(19,6,4,0),(19,6,5,0),(19,6,6,0),(19,6,7,0),(19,6,8,0),(19,6,9,0),(19,6,10,0),(19,6,11,0),(19,7,0,0),(19,7,1,0),(19,7,2,0),(19,7,3,0),(19,7,4,0),(19,7,5,0),(19,7,6,0),(19,7,7,0),(19,7,8,0),(19,7,9,0),(19,7,10,0),(19,7,11,0),(20,0,0,0),(20,0,1,0),(20,0,2,0),(20,0,3,0),(20,0,4,0),(20,0,5,0),(20,0,6,0),(20,0,7,0),(20,0,8,0),(20,0,9,0),(20,0,10,0),(20,0,11,0),(20,1,0,0),(20,1,1,0),(20,1,2,0),(20,1,3,0),(20,1,4,0),(20,1,5,0),(20,1,6,0),(20,1,7,0),(20,1,8,0),(20,1,9,0),(20,1,10,0),(20,1,11,0),(20,2,0,0),(20,2,1,0),(20,2,2,0),(20,2,3,0),(20,2,4,0),(20,2,5,0),(20,2,6,0),(20,2,7,0),(20,2,8,0),(20,2,9,0),(20,2,10,0),(20,2,11,0),(20,3,0,0),(20,3,1,0),(20,3,2,0),(20,3,3,0),(20,3,4,0),(20,3,5,0),(20,3,6,0),(20,3,7,0),(20,3,8,0),(20,3,9,0),(20,3,10,0),(20,3,11,0),(20,4,0,0),(20,4,1,0),(20,4,2,0),(20,4,3,0),(20,4,4,0),(20,4,5,0),(20,4,6,0),(20,4,7,0),(20,4,8,0),(20,4,9,0),(20,4,10,0),(20,4,11,0),(20,5,0,0),(20,5,1,0),(20,5,2,0),(20,5,3,0),(20,5,4,0),(20,5,5,0),(20,5,6,0),(20,5,7,0),(20,5,8,0),(20,5,9,0),(20,5,10,0),(20,5,11,0),(20,6,0,0),(20,6,1,0),(20,6,2,0),(20,6,3,0),(20,6,4,0),(20,6,5,0),(20,6,6,0),(20,6,7,0),(20,6,8,0),(20,6,9,0),(20,6,10,0),(20,6,11,0),(20,7,0,0),(20,7,1,0),(20,7,2,0),(20,7,3,0),(20,7,4,0),(20,7,5,0),(20,7,6,0),(20,7,7,0),(20,7,8,0),(20,7,9,0),(20,7,10,0),(20,7,11,0),(21,0,0,0),(21,0,1,0),(21,0,2,0),(21,0,3,0),(21,0,4,0),(21,0,5,0),(21,0,6,0),(21,0,7,0),(21,0,8,0),(21,0,9,0),(21,0,10,0),(21,0,11,0),(21,1,0,0),(21,1,1,0),(21,1,2,0),(21,1,3,0),(21,1,4,0),(21,1,5,0),(21,1,6,0),(21,1,7,0),(21,1,8,0),(21,1,9,0),(21,1,10,0),(21,1,11,0),(21,2,0,0),(21,2,1,0),(21,2,2,0),(21,2,3,0),(21,2,4,0),(21,2,5,0),(21,2,6,0),(21,2,7,0),(21,2,8,0),(21,2,9,0),(21,2,10,0),(21,2,11,0),(21,3,0,0),(21,3,1,0),(21,3,2,0),(21,3,3,0),(21,3,4,0),(21,3,5,0),(21,3,6,0),(21,3,7,0),(21,3,8,0),(21,3,9,0),(21,3,10,0),(21,3,11,0),(21,4,0,0),(21,4,1,0),(21,4,2,0),(21,4,3,0),(21,4,4,0),(21,4,5,0),(21,4,6,0),(21,4,7,0),(21,4,8,0),(21,4,9,0),(21,4,10,0),(21,4,11,0),(21,5,0,0),(21,5,1,0),(21,5,2,0),(21,5,3,0),(21,5,4,0),(21,5,5,0),(21,5,6,0),(21,5,7,0),(21,5,8,0),(21,5,9,0),(21,5,10,0),(21,5,11,0),(21,6,0,0),(21,6,1,0),(21,6,2,0),(21,6,3,0),(21,6,4,0),(21,6,5,0),(21,6,6,0),(21,6,7,0),(21,6,8,0),(21,6,9,0),(21,6,10,0),(21,6,11,0),(21,7,0,0),(21,7,1,0),(21,7,2,0),(21,7,3,0),(21,7,4,0),(21,7,5,0),(21,7,6,0),(21,7,7,0),(21,7,8,0),(21,7,9,0),(21,7,10,0),(21,7,11,0);
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-21 14:36:59
