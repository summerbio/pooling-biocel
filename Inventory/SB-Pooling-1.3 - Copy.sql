-- MySQL dump 10.13  Distrib 5.7.13, for Win64 (x86_64)
--
-- Host: localhost    Database: velocity11
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `custom_data`
--

DROP TABLE IF EXISTS `custom_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_data` (
  `plate_id` int(11) NOT NULL,
  `custom_key` varchar(255) NOT NULL DEFAULT '',
  `custom_value` varchar(255) NOT NULL DEFAULT '',
  KEY `plate_id` (`plate_id`),
  CONSTRAINT `custom_data_ibfk_1` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_data`
--

LOCK TABLES `custom_data` WRITE;
/*!40000 ALTER TABLE `custom_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(255) NOT NULL DEFAULT '',
  `device_type` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_name` (`device_name`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'1Decapper-1',''),(2,'1Decapper-2',''),(3,'1Decapper-3',''),(4,'1DirectDriveRobot-1',''),(5,'1MicroFill-1',''),(6,'1MiniHub-1','StoreXDevice'),(7,'1Pad-1',''),(8,'1Pad-2',''),(9,'1Pad-3',''),(10,'1VPrep-1',''),(11,'1ZiathScanner-1',''),(12,'Waste Bin - 1',''),(13,'Plate Pad, Standard - 1',''),(14,'Plate Pad, Standard - 2',''),(15,'Plate Pad, Standard - 3',''),(16,'Plate Pad, Standard - 4',''),(17,'Plate Pad, Standard - 5',''),(18,'Plate Pad, Standard - 6',''),(19,'1Conveyor-1',''),(20,'1Conveyor-2',''),(21,'1Conveyor-3',''),(22,'1Conveyor-4',''),(23,'1Conveyor-5',''),(24,'1Conveyor-6',''),(25,'Agilent Phantom Stacker - 1',''),(26,'Agilent Phantom Stacker - 2',''),(27,'Agilent Phantom Stacker - 3',''),(28,'Agilent Phantom Stacker - 4',''),(29,'Agilent Phantom Stacker - 5',''),(30,'Agilent Phantom Stacker - 6',''),(31,'1Decapper-1_Pad',''),(32,'1Decapper-2_Pad',''),(33,'1Decapper-3_Pad',''),(34,'1ACU-1',''),(35,'1Bravo-1',''),(36,'1Bravo-2',''),(37,'1Bravo-3',''),(38,'1Bravo-4',''),(39,'1ExchangerLower',''),(40,'1ExchangerUpper',''),(41,'1HeatedHotel-1',''),(42,'1HeatedHotel-2',''),(43,'1Hotel-1',''),(44,'1Hotel-2',''),(45,'1MicroFill-2',''),(46,'1MicroFill-3',''),(47,'1MicroFill-4',''),(48,'1MultiFlo-1',''),(49,'1Teleshake-1',''),(50,'1Teleshake-2',''),(51,'1Teleshake-3',''),(52,'1Teleshake-4',''),(53,'1Teleshake-5',''),(54,'1Teleshake-6',''),(55,'1Teleshake-7',''),(56,'1Teleshake-8',''),(57,'1Teleshake-9',''),(58,'1Teleshake-10',''),(59,'1Teleshake-11',''),(60,'1Teleshake-12',''),(61,'1Teleshake-13',''),(62,'1Teleshake-14',''),(63,'2AsystRobot-1',''),(64,'2Bravo-1',''),(65,'2Centrifuge-1',''),(66,'2Conveyor-1',''),(67,'2Conveyor-2',''),(68,'2Conveyor-3',''),(69,'2Conveyor-4_Tips',''),(70,'2Conveyor-4_Waste',''),(71,'2Conveyor-5',''),(72,'2Conveyor-6_PCR',''),(73,'2Conveyor-6_Purification',''),(74,'2Conveyor-6_Racks',''),(75,'2Decapper-1',''),(76,'2Decapper-2',''),(77,'2Hotel-1',''),(78,'2Labeler-1',''),(79,'2MicroFill-1',''),(80,'2MultiFlo-1',''),(81,'2Pad-1',''),(82,'2Pad-2',''),(83,'2Pad-3',''),(84,'2Sealer-1',''),(85,'2Stacker-1',''),(86,'2Stacker-2',''),(87,'2Teleshake-1',''),(88,'2ZiathScanner-1',''),(89,'Agilent Phantom Robot',''),(90,'DC24V_PS',''),(91,'Phantom Waste',''),(92,'Shake Subprocess Stacker',''),(93,'VACPUMP_IO',''),(94,'Ziath ZTS-A6 Barcode Scanner - 1',''),(95,'Agilent Phantom Robot - 1',''),(96,'1200PP2',''),(97,'1200PP3',''),(98,'1200PP4 AKA Teleshake 10',''),(99,'1600PP1',''),(100,'1600PP2',''),(101,'1600PP3-CW',''),(102,'1600PP4-PCR',''),(103,'1600PP5-CW',''),(104,'1600PP6',''),(105,'1600PP7-CW',''),(106,'1600PP8 Barcode',''),(107,'Agilent BioCel I/O Interface - 1',''),(108,'Agilent Centrifuge - 1',''),(109,'Agilent Plate Hotel - 1',''),(110,'Agilent Plate Hotel - 2',''),(111,'Agilent Plate Hotel - 3',''),(112,'Asyst 3-Axis Robot - 1',''),(113,'BioCel 1200 DDR',''),(114,'BioTek Liquid Handler - 1',''),(115,'BioTek Liquid Handler - 2',''),(116,'BioTek Liquid Handler - 3',''),(117,'Genetix QFill2 - 1',''),(118,'HandOffHigh',''),(119,'HandOffLow',''),(120,'HandOffMid',''),(121,'Micronic Decapper',''),(122,'Micronic Screw Cap Recapper CS700 - 1',''),(123,'Microscan Barcode Reader - 1',''),(124,'PlateLoc',''),(125,'Teleshake1',''),(126,'Teleshake2',''),(127,'Teleshake3',''),(128,'Teleshake4',''),(129,'Teleshake5',''),(130,'Teleshake6',''),(131,'Teleshake7',''),(132,'Teleshake8',''),(133,'Teleshake9',''),(134,'Teleshake10',''),(135,'VCode',''),(136,'VPrep1',''),(137,'VPrep2',''),(138,'VPrep3',''),(139,'VPrep4',''),(140,'VStack 01',''),(141,'VStack 02',''),(142,'VStack 03',''),(143,'VStack 04',''),(144,'VStack 05',''),(145,'VStack 06',''),(146,'VStack 07',''),(147,'VStack 08',''),(148,'VStack 09',''),(149,'VStack 10','');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_data`
--

DROP TABLE IF EXISTS `global_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_data` (
  `global_key` varchar(255) NOT NULL,
  `global_value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`global_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_data`
--

LOCK TABLES `global_data` WRITE;
/*!40000 ALTER TABLE `global_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `global_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `device_specific_location_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `0_15` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4420 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (4411,6,1),(4412,6,2),(4413,6,3),(4414,6,4),(4415,6,5),(4416,6,6),(4417,6,7),(4418,6,8),(4419,6,9);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plategroup_names`
--

DROP TABLE IF EXISTS `plategroup_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plategroup_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plategroup_names`
--

LOCK TABLES `plategroup_names` WRITE;
/*!40000 ALTER TABLE `plategroup_names` DISABLE KEYS */;
INSERT INTO `plategroup_names` VALUES (1,1,'Discrete Racks'),(2,1,'Pooled Racks'),(3,1,'Discrete Tip Boxes');
/*!40000 ALTER TABLE `plategroup_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plategroups`
--

DROP TABLE IF EXISTS `plategroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plategroups` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `plate_id` int(11) DEFAULT '0',
  `plate_index` int(11) DEFAULT '0',
  `cassette` int(11) DEFAULT '0',
  `slot` int(11) DEFAULT '0',
  `device_name` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `group_id_2` (`group_id`,`plate_index`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `plategroups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `plategroup_names` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plategroups`
--

LOCK TABLES `plategroups` WRITE;
/*!40000 ALTER TABLE `plategroups` DISABLE KEYS */;
INSERT INTO `plategroups` VALUES (1,5599,0,-1,-1,'1MiniHub-1'),(1,5600,1,-1,-1,'1MiniHub-1'),(1,5601,2,-1,-1,'1MiniHub-1'),(1,5602,3,-1,-1,'1MiniHub-1'),(1,5603,4,-1,-1,'1MiniHub-1'),(3,5604,0,-1,-1,'1MiniHub-1'),(3,5605,1,-1,-1,'1MiniHub-1'),(2,5606,0,-1,-1,'1MiniHub-1'),(2,5607,1,-1,-1,'1MiniHub-1');
/*!40000 ALTER TABLE `plategroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plates`
--

DROP TABLE IF EXISTS `plates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate_name` varchar(255) NOT NULL DEFAULT '',
  `labware` varchar(255) NOT NULL DEFAULT '',
  `northbc` varchar(255) NOT NULL DEFAULT '',
  `eastbc` varchar(255) NOT NULL DEFAULT '',
  `southbc` varchar(255) NOT NULL DEFAULT '',
  `westbc` varchar(255) NOT NULL DEFAULT '',
  `parent` int(11) NOT NULL DEFAULT '0',
  `destination_location_id` int(11) NOT NULL DEFAULT '0',
  `current_location_id` int(11) NOT NULL DEFAULT '0',
  `protocol_id` int(11) NOT NULL DEFAULT '0',
  `sealed` int(11) NOT NULL DEFAULT '0',
  `lidded` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5608 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plates`
--

LOCK TABLES `plates` WRITE;
/*!40000 ALTER TABLE `plates` DISABLE KEYS */;
INSERT INTO `plates` VALUES (5599,'Discrete Rack 1','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4411,0,0,0,0,'2021-09-13 23:49:01.451839'),(5600,'Discrete Rack 2','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4412,0,0,0,0,'2021-09-13 23:49:21.227715'),(5601,'Discrete Rack 3','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4413,0,0,0,0,'2021-09-13 23:49:39.089944'),(5602,'Discrete Rack 4','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4414,0,0,0,0,'2021-09-13 23:49:56.876012'),(5603,'Discrete Rack 5','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4415,0,0,0,0,'2021-09-13 23:50:19.197428'),(5604,'Discrete Tips \"1\"','96 180ul Filtered Delidded','','','','',0,0,4416,0,0,0,0,'2021-09-13 23:51:12.761542'),(5605,'Discrete Tips \"2\"','96 180ul Filtered Delidded','','','','',0,0,4417,0,0,0,0,'2021-09-13 23:51:37.730496'),(5606,'Pooled Rack 1','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4418,0,0,0,0,'2021-09-13 23:51:54.719297'),(5607,'Pooled Rack 2','96 1.4ml Micronic Tube Rack External Thread WITH Caps','','','','',0,0,4419,0,0,0,0,'2021-09-13 23:52:19.175837');
/*!40000 ALTER TABLE `plates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protocol_steps`
--

DROP TABLE IF EXISTS `protocol_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protocol_steps` (
  `plate_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `step` int(11) NOT NULL DEFAULT '0',
  `substep` int(11) NOT NULL DEFAULT '0',
  `start_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  UNIQUE KEY `plate_id` (`plate_id`),
  CONSTRAINT `0_17` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protocol_steps`
--

LOCK TABLES `protocol_steps` WRITE;
/*!40000 ALTER TABLE `protocol_steps` DISABLE KEYS */;
INSERT INTO `protocol_steps` VALUES (5599,0,0,0,'2021-09-13 23:49:01.451839'),(5600,0,0,0,'2021-09-13 23:49:21.227715'),(5601,0,0,0,'2021-09-13 23:49:39.089944'),(5602,0,0,0,'2021-09-13 23:49:56.876012'),(5603,0,0,0,'2021-09-13 23:50:19.197428'),(5604,0,0,0,'2021-09-13 23:51:12.745949'),(5605,0,0,0,'2021-09-13 23:51:37.730496'),(5606,0,0,0,'2021-09-13 23:51:54.703912'),(5607,0,0,0,'2021-09-13 23:52:19.175837');
/*!40000 ALTER TABLE `protocol_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protocols`
--

DROP TABLE IF EXISTS `protocols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protocols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_file` varchar(255) NOT NULL DEFAULT '',
  `protocol_file` varchar(255) NOT NULL DEFAULT '',
  `start_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `device_file` (`device_file`),
  UNIQUE KEY `protocol_file` (`protocol_file`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protocols`
--

LOCK TABLES `protocols` WRITE;
/*!40000 ALTER TABLE `protocols` DISABLE KEYS */;
/*!40000 ALTER TABLE `protocols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storexdevice`
--

DROP TABLE IF EXISTS `storexdevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storexdevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `cassette` int(11) NOT NULL DEFAULT '0',
  `slot` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `0_20` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storexdevice`
--

LOCK TABLES `storexdevice` WRITE;
/*!40000 ALTER TABLE `storexdevice` DISABLE KEYS */;
INSERT INTO `storexdevice` VALUES (1,6,1,1),(2,6,1,2),(3,6,1,3),(4,6,1,4),(5,6,1,5),(6,6,2,1),(7,6,2,2),(8,6,3,1),(9,6,3,2);
/*!40000 ALTER TABLE `storexdevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumes` (
  `plate_id` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `volume` double NOT NULL,
  KEY `plate_id` (`plate_id`),
  CONSTRAINT `0_22` FOREIGN KEY (`plate_id`) REFERENCES `plates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
INSERT INTO `volumes` VALUES (5599,0,0,0),(5599,0,1,0),(5599,0,2,0),(5599,0,3,0),(5599,0,4,0),(5599,0,5,0),(5599,0,6,0),(5599,0,7,0),(5599,0,8,0),(5599,0,9,0),(5599,0,10,0),(5599,0,11,0),(5599,1,0,0),(5599,1,1,0),(5599,1,2,0),(5599,1,3,0),(5599,1,4,0),(5599,1,5,0),(5599,1,6,0),(5599,1,7,0),(5599,1,8,0),(5599,1,9,0),(5599,1,10,0),(5599,1,11,0),(5599,2,0,0),(5599,2,1,0),(5599,2,2,0),(5599,2,3,0),(5599,2,4,0),(5599,2,5,0),(5599,2,6,0),(5599,2,7,0),(5599,2,8,0),(5599,2,9,0),(5599,2,10,0),(5599,2,11,0),(5599,3,0,0),(5599,3,1,0),(5599,3,2,0),(5599,3,3,0),(5599,3,4,0),(5599,3,5,0),(5599,3,6,0),(5599,3,7,0),(5599,3,8,0),(5599,3,9,0),(5599,3,10,0),(5599,3,11,0),(5599,4,0,0),(5599,4,1,0),(5599,4,2,0),(5599,4,3,0),(5599,4,4,0),(5599,4,5,0),(5599,4,6,0),(5599,4,7,0),(5599,4,8,0),(5599,4,9,0),(5599,4,10,0),(5599,4,11,0),(5599,5,0,0),(5599,5,1,0),(5599,5,2,0),(5599,5,3,0),(5599,5,4,0),(5599,5,5,0),(5599,5,6,0),(5599,5,7,0),(5599,5,8,0),(5599,5,9,0),(5599,5,10,0),(5599,5,11,0),(5599,6,0,0),(5599,6,1,0),(5599,6,2,0),(5599,6,3,0),(5599,6,4,0),(5599,6,5,0),(5599,6,6,0),(5599,6,7,0),(5599,6,8,0),(5599,6,9,0),(5599,6,10,0),(5599,6,11,0),(5599,7,0,0),(5599,7,1,0),(5599,7,2,0),(5599,7,3,0),(5599,7,4,0),(5599,7,5,0),(5599,7,6,0),(5599,7,7,0),(5599,7,8,0),(5599,7,9,0),(5599,7,10,0),(5599,7,11,0),(5600,0,0,0),(5600,0,1,0),(5600,0,2,0),(5600,0,3,0),(5600,0,4,0),(5600,0,5,0),(5600,0,6,0),(5600,0,7,0),(5600,0,8,0),(5600,0,9,0),(5600,0,10,0),(5600,0,11,0),(5600,1,0,0),(5600,1,1,0),(5600,1,2,0),(5600,1,3,0),(5600,1,4,0),(5600,1,5,0),(5600,1,6,0),(5600,1,7,0),(5600,1,8,0),(5600,1,9,0),(5600,1,10,0),(5600,1,11,0),(5600,2,0,0),(5600,2,1,0),(5600,2,2,0),(5600,2,3,0),(5600,2,4,0),(5600,2,5,0),(5600,2,6,0),(5600,2,7,0),(5600,2,8,0),(5600,2,9,0),(5600,2,10,0),(5600,2,11,0),(5600,3,0,0),(5600,3,1,0),(5600,3,2,0),(5600,3,3,0),(5600,3,4,0),(5600,3,5,0),(5600,3,6,0),(5600,3,7,0),(5600,3,8,0),(5600,3,9,0),(5600,3,10,0),(5600,3,11,0),(5600,4,0,0),(5600,4,1,0),(5600,4,2,0),(5600,4,3,0),(5600,4,4,0),(5600,4,5,0),(5600,4,6,0),(5600,4,7,0),(5600,4,8,0),(5600,4,9,0),(5600,4,10,0),(5600,4,11,0),(5600,5,0,0),(5600,5,1,0),(5600,5,2,0),(5600,5,3,0),(5600,5,4,0),(5600,5,5,0),(5600,5,6,0),(5600,5,7,0),(5600,5,8,0),(5600,5,9,0),(5600,5,10,0),(5600,5,11,0),(5600,6,0,0),(5600,6,1,0),(5600,6,2,0),(5600,6,3,0),(5600,6,4,0),(5600,6,5,0),(5600,6,6,0),(5600,6,7,0),(5600,6,8,0),(5600,6,9,0),(5600,6,10,0),(5600,6,11,0),(5600,7,0,0),(5600,7,1,0),(5600,7,2,0),(5600,7,3,0),(5600,7,4,0),(5600,7,5,0),(5600,7,6,0),(5600,7,7,0),(5600,7,8,0),(5600,7,9,0),(5600,7,10,0),(5600,7,11,0),(5601,0,0,0),(5601,0,1,0),(5601,0,2,0),(5601,0,3,0),(5601,0,4,0),(5601,0,5,0),(5601,0,6,0),(5601,0,7,0),(5601,0,8,0),(5601,0,9,0),(5601,0,10,0),(5601,0,11,0),(5601,1,0,0),(5601,1,1,0),(5601,1,2,0),(5601,1,3,0),(5601,1,4,0),(5601,1,5,0),(5601,1,6,0),(5601,1,7,0),(5601,1,8,0),(5601,1,9,0),(5601,1,10,0),(5601,1,11,0),(5601,2,0,0),(5601,2,1,0),(5601,2,2,0),(5601,2,3,0),(5601,2,4,0),(5601,2,5,0),(5601,2,6,0),(5601,2,7,0),(5601,2,8,0),(5601,2,9,0),(5601,2,10,0),(5601,2,11,0),(5601,3,0,0),(5601,3,1,0),(5601,3,2,0),(5601,3,3,0),(5601,3,4,0),(5601,3,5,0),(5601,3,6,0),(5601,3,7,0),(5601,3,8,0),(5601,3,9,0),(5601,3,10,0),(5601,3,11,0),(5601,4,0,0),(5601,4,1,0),(5601,4,2,0),(5601,4,3,0),(5601,4,4,0),(5601,4,5,0),(5601,4,6,0),(5601,4,7,0),(5601,4,8,0),(5601,4,9,0),(5601,4,10,0),(5601,4,11,0),(5601,5,0,0),(5601,5,1,0),(5601,5,2,0),(5601,5,3,0),(5601,5,4,0),(5601,5,5,0),(5601,5,6,0),(5601,5,7,0),(5601,5,8,0),(5601,5,9,0),(5601,5,10,0),(5601,5,11,0),(5601,6,0,0),(5601,6,1,0),(5601,6,2,0),(5601,6,3,0),(5601,6,4,0),(5601,6,5,0),(5601,6,6,0),(5601,6,7,0),(5601,6,8,0),(5601,6,9,0),(5601,6,10,0),(5601,6,11,0),(5601,7,0,0),(5601,7,1,0),(5601,7,2,0),(5601,7,3,0),(5601,7,4,0),(5601,7,5,0),(5601,7,6,0),(5601,7,7,0),(5601,7,8,0),(5601,7,9,0),(5601,7,10,0),(5601,7,11,0),(5602,0,0,0),(5602,0,1,0),(5602,0,2,0),(5602,0,3,0),(5602,0,4,0),(5602,0,5,0),(5602,0,6,0),(5602,0,7,0),(5602,0,8,0),(5602,0,9,0),(5602,0,10,0),(5602,0,11,0),(5602,1,0,0),(5602,1,1,0),(5602,1,2,0),(5602,1,3,0),(5602,1,4,0),(5602,1,5,0),(5602,1,6,0),(5602,1,7,0),(5602,1,8,0),(5602,1,9,0),(5602,1,10,0),(5602,1,11,0),(5602,2,0,0),(5602,2,1,0),(5602,2,2,0),(5602,2,3,0),(5602,2,4,0),(5602,2,5,0),(5602,2,6,0),(5602,2,7,0),(5602,2,8,0),(5602,2,9,0),(5602,2,10,0),(5602,2,11,0),(5602,3,0,0),(5602,3,1,0),(5602,3,2,0),(5602,3,3,0),(5602,3,4,0),(5602,3,5,0),(5602,3,6,0),(5602,3,7,0),(5602,3,8,0),(5602,3,9,0),(5602,3,10,0),(5602,3,11,0),(5602,4,0,0),(5602,4,1,0),(5602,4,2,0),(5602,4,3,0),(5602,4,4,0),(5602,4,5,0),(5602,4,6,0),(5602,4,7,0),(5602,4,8,0),(5602,4,9,0),(5602,4,10,0),(5602,4,11,0),(5602,5,0,0),(5602,5,1,0),(5602,5,2,0),(5602,5,3,0),(5602,5,4,0),(5602,5,5,0),(5602,5,6,0),(5602,5,7,0),(5602,5,8,0),(5602,5,9,0),(5602,5,10,0),(5602,5,11,0),(5602,6,0,0),(5602,6,1,0),(5602,6,2,0),(5602,6,3,0),(5602,6,4,0),(5602,6,5,0),(5602,6,6,0),(5602,6,7,0),(5602,6,8,0),(5602,6,9,0),(5602,6,10,0),(5602,6,11,0),(5602,7,0,0),(5602,7,1,0),(5602,7,2,0),(5602,7,3,0),(5602,7,4,0),(5602,7,5,0),(5602,7,6,0),(5602,7,7,0),(5602,7,8,0),(5602,7,9,0),(5602,7,10,0),(5602,7,11,0),(5603,0,0,0),(5603,0,1,0),(5603,0,2,0),(5603,0,3,0),(5603,0,4,0),(5603,0,5,0),(5603,0,6,0),(5603,0,7,0),(5603,0,8,0),(5603,0,9,0),(5603,0,10,0),(5603,0,11,0),(5603,1,0,0),(5603,1,1,0),(5603,1,2,0),(5603,1,3,0),(5603,1,4,0),(5603,1,5,0),(5603,1,6,0),(5603,1,7,0),(5603,1,8,0),(5603,1,9,0),(5603,1,10,0),(5603,1,11,0),(5603,2,0,0),(5603,2,1,0),(5603,2,2,0),(5603,2,3,0),(5603,2,4,0),(5603,2,5,0),(5603,2,6,0),(5603,2,7,0),(5603,2,8,0),(5603,2,9,0),(5603,2,10,0),(5603,2,11,0),(5603,3,0,0),(5603,3,1,0),(5603,3,2,0),(5603,3,3,0),(5603,3,4,0),(5603,3,5,0),(5603,3,6,0),(5603,3,7,0),(5603,3,8,0),(5603,3,9,0),(5603,3,10,0),(5603,3,11,0),(5603,4,0,0),(5603,4,1,0),(5603,4,2,0),(5603,4,3,0),(5603,4,4,0),(5603,4,5,0),(5603,4,6,0),(5603,4,7,0),(5603,4,8,0),(5603,4,9,0),(5603,4,10,0),(5603,4,11,0),(5603,5,0,0),(5603,5,1,0),(5603,5,2,0),(5603,5,3,0),(5603,5,4,0),(5603,5,5,0),(5603,5,6,0),(5603,5,7,0),(5603,5,8,0),(5603,5,9,0),(5603,5,10,0),(5603,5,11,0),(5603,6,0,0),(5603,6,1,0),(5603,6,2,0),(5603,6,3,0),(5603,6,4,0),(5603,6,5,0),(5603,6,6,0),(5603,6,7,0),(5603,6,8,0),(5603,6,9,0),(5603,6,10,0),(5603,6,11,0),(5603,7,0,0),(5603,7,1,0),(5603,7,2,0),(5603,7,3,0),(5603,7,4,0),(5603,7,5,0),(5603,7,6,0),(5603,7,7,0),(5603,7,8,0),(5603,7,9,0),(5603,7,10,0),(5603,7,11,0),(5604,0,0,0),(5604,0,1,0),(5604,0,2,0),(5604,0,3,0),(5604,0,4,0),(5604,0,5,0),(5604,0,6,0),(5604,0,7,0),(5604,0,8,0),(5604,0,9,0),(5604,0,10,0),(5604,0,11,0),(5604,1,0,0),(5604,1,1,0),(5604,1,2,0),(5604,1,3,0),(5604,1,4,0),(5604,1,5,0),(5604,1,6,0),(5604,1,7,0),(5604,1,8,0),(5604,1,9,0),(5604,1,10,0),(5604,1,11,0),(5604,2,0,0),(5604,2,1,0),(5604,2,2,0),(5604,2,3,0),(5604,2,4,0),(5604,2,5,0),(5604,2,6,0),(5604,2,7,0),(5604,2,8,0),(5604,2,9,0),(5604,2,10,0),(5604,2,11,0),(5604,3,0,0),(5604,3,1,0),(5604,3,2,0),(5604,3,3,0),(5604,3,4,0),(5604,3,5,0),(5604,3,6,0),(5604,3,7,0),(5604,3,8,0),(5604,3,9,0),(5604,3,10,0),(5604,3,11,0),(5604,4,0,0),(5604,4,1,0),(5604,4,2,0),(5604,4,3,0),(5604,4,4,0),(5604,4,5,0),(5604,4,6,0),(5604,4,7,0),(5604,4,8,0),(5604,4,9,0),(5604,4,10,0),(5604,4,11,0),(5604,5,0,0),(5604,5,1,0),(5604,5,2,0),(5604,5,3,0),(5604,5,4,0),(5604,5,5,0),(5604,5,6,0),(5604,5,7,0),(5604,5,8,0),(5604,5,9,0),(5604,5,10,0),(5604,5,11,0),(5604,6,0,0),(5604,6,1,0),(5604,6,2,0),(5604,6,3,0),(5604,6,4,0),(5604,6,5,0),(5604,6,6,0),(5604,6,7,0),(5604,6,8,0),(5604,6,9,0),(5604,6,10,0),(5604,6,11,0),(5604,7,0,0),(5604,7,1,0),(5604,7,2,0),(5604,7,3,0),(5604,7,4,0),(5604,7,5,0),(5604,7,6,0),(5604,7,7,0),(5604,7,8,0),(5604,7,9,0),(5604,7,10,0),(5604,7,11,0),(5605,0,0,0),(5605,0,1,0),(5605,0,2,0),(5605,0,3,0),(5605,0,4,0),(5605,0,5,0),(5605,0,6,0),(5605,0,7,0),(5605,0,8,0),(5605,0,9,0),(5605,0,10,0),(5605,0,11,0),(5605,1,0,0),(5605,1,1,0),(5605,1,2,0),(5605,1,3,0),(5605,1,4,0),(5605,1,5,0),(5605,1,6,0),(5605,1,7,0),(5605,1,8,0),(5605,1,9,0),(5605,1,10,0),(5605,1,11,0),(5605,2,0,0),(5605,2,1,0),(5605,2,2,0),(5605,2,3,0),(5605,2,4,0),(5605,2,5,0),(5605,2,6,0),(5605,2,7,0),(5605,2,8,0),(5605,2,9,0),(5605,2,10,0),(5605,2,11,0),(5605,3,0,0),(5605,3,1,0),(5605,3,2,0),(5605,3,3,0),(5605,3,4,0),(5605,3,5,0),(5605,3,6,0),(5605,3,7,0),(5605,3,8,0),(5605,3,9,0),(5605,3,10,0),(5605,3,11,0),(5605,4,0,0),(5605,4,1,0),(5605,4,2,0),(5605,4,3,0),(5605,4,4,0),(5605,4,5,0),(5605,4,6,0),(5605,4,7,0),(5605,4,8,0),(5605,4,9,0),(5605,4,10,0),(5605,4,11,0),(5605,5,0,0),(5605,5,1,0),(5605,5,2,0),(5605,5,3,0),(5605,5,4,0),(5605,5,5,0),(5605,5,6,0),(5605,5,7,0),(5605,5,8,0),(5605,5,9,0),(5605,5,10,0),(5605,5,11,0),(5605,6,0,0),(5605,6,1,0),(5605,6,2,0),(5605,6,3,0),(5605,6,4,0),(5605,6,5,0),(5605,6,6,0),(5605,6,7,0),(5605,6,8,0),(5605,6,9,0),(5605,6,10,0),(5605,6,11,0),(5605,7,0,0),(5605,7,1,0),(5605,7,2,0),(5605,7,3,0),(5605,7,4,0),(5605,7,5,0),(5605,7,6,0),(5605,7,7,0),(5605,7,8,0),(5605,7,9,0),(5605,7,10,0),(5605,7,11,0),(5606,0,0,0),(5606,0,1,0),(5606,0,2,0),(5606,0,3,0),(5606,0,4,0),(5606,0,5,0),(5606,0,6,0),(5606,0,7,0),(5606,0,8,0),(5606,0,9,0),(5606,0,10,0),(5606,0,11,0),(5606,1,0,0),(5606,1,1,0),(5606,1,2,0),(5606,1,3,0),(5606,1,4,0),(5606,1,5,0),(5606,1,6,0),(5606,1,7,0),(5606,1,8,0),(5606,1,9,0),(5606,1,10,0),(5606,1,11,0),(5606,2,0,0),(5606,2,1,0),(5606,2,2,0),(5606,2,3,0),(5606,2,4,0),(5606,2,5,0),(5606,2,6,0),(5606,2,7,0),(5606,2,8,0),(5606,2,9,0),(5606,2,10,0),(5606,2,11,0),(5606,3,0,0),(5606,3,1,0),(5606,3,2,0),(5606,3,3,0),(5606,3,4,0),(5606,3,5,0),(5606,3,6,0),(5606,3,7,0),(5606,3,8,0),(5606,3,9,0),(5606,3,10,0),(5606,3,11,0),(5606,4,0,0),(5606,4,1,0),(5606,4,2,0),(5606,4,3,0),(5606,4,4,0),(5606,4,5,0),(5606,4,6,0),(5606,4,7,0),(5606,4,8,0),(5606,4,9,0),(5606,4,10,0),(5606,4,11,0),(5606,5,0,0),(5606,5,1,0),(5606,5,2,0),(5606,5,3,0),(5606,5,4,0),(5606,5,5,0),(5606,5,6,0),(5606,5,7,0),(5606,5,8,0),(5606,5,9,0),(5606,5,10,0),(5606,5,11,0),(5606,6,0,0),(5606,6,1,0),(5606,6,2,0),(5606,6,3,0),(5606,6,4,0),(5606,6,5,0),(5606,6,6,0),(5606,6,7,0),(5606,6,8,0),(5606,6,9,0),(5606,6,10,0),(5606,6,11,0),(5606,7,0,0),(5606,7,1,0),(5606,7,2,0),(5606,7,3,0),(5606,7,4,0),(5606,7,5,0),(5606,7,6,0),(5606,7,7,0),(5606,7,8,0),(5606,7,9,0),(5606,7,10,0),(5606,7,11,0),(5607,0,0,0),(5607,0,1,0),(5607,0,2,0),(5607,0,3,0),(5607,0,4,0),(5607,0,5,0),(5607,0,6,0),(5607,0,7,0),(5607,0,8,0),(5607,0,9,0),(5607,0,10,0),(5607,0,11,0),(5607,1,0,0),(5607,1,1,0),(5607,1,2,0),(5607,1,3,0),(5607,1,4,0),(5607,1,5,0),(5607,1,6,0),(5607,1,7,0),(5607,1,8,0),(5607,1,9,0),(5607,1,10,0),(5607,1,11,0),(5607,2,0,0),(5607,2,1,0),(5607,2,2,0),(5607,2,3,0),(5607,2,4,0),(5607,2,5,0),(5607,2,6,0),(5607,2,7,0),(5607,2,8,0),(5607,2,9,0),(5607,2,10,0),(5607,2,11,0),(5607,3,0,0),(5607,3,1,0),(5607,3,2,0),(5607,3,3,0),(5607,3,4,0),(5607,3,5,0),(5607,3,6,0),(5607,3,7,0),(5607,3,8,0),(5607,3,9,0),(5607,3,10,0),(5607,3,11,0),(5607,4,0,0),(5607,4,1,0),(5607,4,2,0),(5607,4,3,0),(5607,4,4,0),(5607,4,5,0),(5607,4,6,0),(5607,4,7,0),(5607,4,8,0),(5607,4,9,0),(5607,4,10,0),(5607,4,11,0),(5607,5,0,0),(5607,5,1,0),(5607,5,2,0),(5607,5,3,0),(5607,5,4,0),(5607,5,5,0),(5607,5,6,0),(5607,5,7,0),(5607,5,8,0),(5607,5,9,0),(5607,5,10,0),(5607,5,11,0),(5607,6,0,0),(5607,6,1,0),(5607,6,2,0),(5607,6,3,0),(5607,6,4,0),(5607,6,5,0),(5607,6,6,0),(5607,6,7,0),(5607,6,8,0),(5607,6,9,0),(5607,6,10,0),(5607,6,11,0),(5607,7,0,0),(5607,7,1,0),(5607,7,2,0),(5607,7,3,0),(5607,7,4,0),(5607,7,5,0),(5607,7,6,0),(5607,7,7,0),(5607,7,8,0),(5607,7,9,0),(5607,7,10,0),(5607,7,11,0);
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-15 12:10:01
